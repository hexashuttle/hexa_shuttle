package notif;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import config.ShuttleConfig;
import request.NotifDetails;

public class InvokePush {
	
	public void pushMessage(NotifDetails notifDetailObj,String parent_fcm_mobile_token,String action){

        System.out.println("Push to Passenger");
        String icon="myicon";
		String authorization="key="+notifDetailObj.getPush_fcm_key();
		String title="";
		if(action.equals(ShuttleConfig.BOOKING_ACTION)){
			title=ShuttleConfig.BOOKING_PUSH_MSG_TITLE;
		}else if(action.equals(ShuttleConfig.CANCEL_BOOKING_ACTION)){
			title=ShuttleConfig.CANCEL_BOOKING_PUSH_MSG_TITLE;
		}

        try{
            String urlForPush= notifDetailObj.getPush_url();
            String postBodyForPushMessage="{\"to\":\""+parent_fcm_mobile_token+"\","
					+ "\"notification\": {"
							+ "\"body\":\""+notifDetailObj.getCommonBody()+"\","
							+ "\"title\":\""+title+"\","
							+ "\"icon\":\""+icon+"\""
					+ "},"
					+ "\"priority\" : 10"
				+ "}";
            System.out.println("urlForPush : "+urlForPush);
            System.out.println("postBodyForPushMessage : "+postBodyForPushMessage);
            System.out.println("authorization : "+authorization);
            HttpResponse<String> response = Unirest.post(urlForPush)
                    .header("Authorization", authorization)
                    .header("content-type", "application/json")
                    .body(postBodyForPushMessage)
                    .asString();
            System.out.println("pushToParent response Status"+response.getStatus());
            System.out.println("pushToParent response Body"+response.getBody());
            System.out.println("Headers : "+response.getHeaders());
            if(response.getStatus()==200){
                System.out.println("pushToParent Success!!!");
            }else{
                System.out.println("pushToParent fail.");
            }
        }catch(UnirestException e){
            System.out.println("UnirestException at pushMessage : "+e.getMessage());
        }
	}
	
	public void pushMessageToDriver(String push_body,
			String push_title,
			String action,
			String push_fcm_key,
			String name,
			String push_url,
			String driver_push_token
			){


        System.out.println("pushMessageToDriver ");
        String icon="myicon";
		String authorization="key="+push_fcm_key;

        try{
            String postBodyForPushMessage="{\"to\":\""+driver_push_token+"\","
					+ "\"notification\": {"
							+ "\"body\":\""+push_body+"\","
							+ "\"title\":\""+push_title+"\","
							+ "\"icon\":\""+icon+"\""
					+ "},"
					+ "\"data\": {"
						+ "\"name\":\""+name+"\","
						+ "\"action\":\""+action+"\""
					+ "},"
					+ "\"priority\" : 10"
				+ "}";
            System.out.println("urlForPush : "+push_url);
            System.out.println("postBodyForPushMessage : "+postBodyForPushMessage);
            System.out.println("authorization : "+authorization);
            HttpResponse<String> response = Unirest.post(push_url)
                    .header("Authorization", authorization)
                    .header("content-type", "application/json")
                    .body(postBodyForPushMessage)
                    .asString();
            System.out.println("pushMessageToDriver response Status"+response.getStatus());
            System.out.println("pushMessageToDriver response Body"+response.getBody());
            System.out.println("Headers : "+response.getHeaders());
            if(response.getStatus()==200){
                System.out.println("pushMessageToDriver Success!!!");
            }else{
                System.out.println("pushMessageToDriver fail.");
            }
        }catch(UnirestException e){
            System.out.println("UnirestException at pushMessage : "+e.getMessage());
        }
	
		
	}

}

package notif;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import request.NotifDetails;

public class InvokeSms {
	
	public void sendSms(NotifDetails smsDetailObj){
		try {
			String encodedMsgUrl = URLEncoder.encode(smsDetailObj.getCommonBody().toString(), "UTF-8").replace("+", "%20");
			String url=smsDetailObj.getSms_url()+"?AUTH_KEY="+smsDetailObj.getSms_auth_key();
		       url += "&message="+ encodedMsgUrl + 
		    		  "&senderId="+smsDetailObj.getSms_senderId()+
		    		  "&routeId="+smsDetailObj.getSms_routeId()+
		    		  "&mobileNos=" + smsDetailObj.getMobileNos() + 
		    		  "&smsContentType="+smsDetailObj.getSmsContentType();
		       System.out.println("url : "+url);
		       HttpResponse<String> response = Unirest.get(url)
	                    .header("content-type", "application/x-www-form-urlencoded")
	                    .header("cache-control", "no-cache")
	                    .asString();
	            System.out.println("sendSms response Status"+response.getStatus());
	            System.out.println("sendSms response Body"+response.getBody());
	            if(response.getStatus()==200){
	                System.out.println("sms sent successfully.");
	            }else{
	                System.out.println("sms failed.");
	            }
		} catch (UnsupportedEncodingException uee) {
			// TODO Auto-generated catch block
			uee.printStackTrace();
			System.err.println("UnsupportedEncodingException at sendSms :"+uee.getMessage());
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Exception at sendSms :"+e.getMessage());
		} 
	}
	/**
	public void sendSMSMessage() {
		@SuppressWarnings("deprecation")
		AmazonSNSClient snsClient = new AmazonSNSClient();
        String message = "My SMS message";
        String phoneNumber = "+919007040781";
        
        Map<String, MessageAttributeValue> smsAttributes =
                new HashMap<String, MessageAttributeValue>();
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue("mySenderID") //The sender ID shown on the device.
                .withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue()
                .withStringValue("0.50") //Sets the max price to 0.50 USD.
                .withDataType("Number"));
        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                .withStringValue("Promotional") //Sets the type to promotional.
                .withDataType("String"));
        
        //<set SMS attributes>
	    PublishResult result = snsClient.publish(new PublishRequest()
	                        .withMessage(message)
	                        .withPhoneNumber(phoneNumber)
	                        .withMessageAttributes(smsAttributes));
	    System.out.println("Result : "+result); // Prints the message ID.
	}**/
	

}

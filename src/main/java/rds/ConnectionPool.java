package rds;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * Created by satya on 15/2/18.
 */

public class ConnectionPool {

    // JDBC Driver Name & Database URL
    static final String JDBC_DRIVER = "org.postgresql.Driver";
    //static final String JDBC_DB_URL =System.getenv("READ_PG_HOST"); //"jdbc:postgresql://hexa.crhsa2hfbow2.ap-southeast-1.rds.amazonaws.com:5432/dbhex";


    static final String JDBC_DB_URL =getHost();
    // JDBC Database Credentials
    static final String JDBC_USER = getUserName();
    static final String JDBC_PASS = getPwd();

    private static GenericObjectPool gPool = null;

    private static String getHost(){
        String host = System.getenv("PG_HOST");
        System.out.println("****> PG_HOST : "+host);
        if(host==null || host.equals("") || host.equals("null")){
            host="jdbc:postgresql://shuttle-dev.cvqubxx1x2et.ap-south-1.rds.amazonaws.com/dev_shuttle_db";
        }
        return host;

    }
    private static String getUserName(){
        String uname =System.getenv("PG_USERNAME");
        System.out.println("***>PG_USERNAME : "+uname);
        if(uname==null || uname.equals("") || uname.equals("null")){
            uname="dbhexdev";
        }
        return uname;
    }

    private static String getPwd(){
        String pwd = System.getenv("PG_PASSWORD");
        System.out.println("*****> PG_PASSWORD : "+pwd);
        if(pwd==null || pwd.equals("") || pwd.equals("null")){
            pwd="Patdbhexdev";
        }
        return pwd;
    }

    @SuppressWarnings("unused")
    public DataSource setUpPool() throws Exception {
        Class.forName(JDBC_DRIVER);

        // Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object!
        gPool = new GenericObjectPool();
        String pool_size=System.getenv("JDBC_MAX_POOL_SIZE");
        System.out.println("pool_size : "+pool_size);
        if(pool_size==null || pool_size.equals("") || pool_size.equals("null")){
            pool_size="1";
        }
        gPool.setMaxActive(Integer.parseInt(pool_size));
        // Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object!
        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

        // Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality!
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
        return new PoolingDataSource(gPool);
    }

    public GenericObjectPool getConnectionPool() {
        return gPool;
    }

    // This Method Is Used To Print The Connection Pool Status
    public void printDbStatus() {
        System.out.println("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }
}


package request;

public class BookingRequest {
	private String passenger_id;
	private String trip_id;
	private String pick_stop_id;
	private String 	drop_stop_id;
	private String number_of_seat;
	private String email;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	//Added for payment 
	private String order_id;
	 private String cust_id;
	 private String txn_amount;
	 private String customer_mobile;
	 private String customer_email;
	 **/
	
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	
	public String getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	public String getPick_stop_id() {
		return pick_stop_id;
	}
	public void setPick_stop_id(String pick_stop_id) {
		this.pick_stop_id = pick_stop_id;
	}
	public String getDrop_stop_id() {
		return drop_stop_id;
	}
	public void setDrop_stop_id(String drop_stop_id) {
		this.drop_stop_id = drop_stop_id;
	}
	public String getNumber_of_seat() {
		return number_of_seat;
	}
	public void setNumber_of_seat(String number_of_seat) {
		this.number_of_seat = number_of_seat;
	}
	/**
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getTxn_amount() {
		return txn_amount;
	}
	public void setTxn_amount(String txn_amount) {
		this.txn_amount = txn_amount;
	}
	public String getCustomer_mobile() {
		return customer_mobile;
	}
	public void setCustomer_mobile(String customer_mobile) {
		this.customer_mobile = customer_mobile;
	}
	public String getCustomer_email() {
		return customer_email;
	}
	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}**/
	
	

}

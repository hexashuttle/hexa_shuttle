package request;

public class DeviceInfoRequest {
	
	private String imei1;
	private String imei2;
	private String user_id;
	private String push_token;
	private String device_uid1;
	private String device_uid2;
	private String os_type;
	private String os_version;
	
	private String app_version;
	private String model;
	private String carrier;
	
	private String email;

	public String getImei1() {
		return imei1;
	}

	public void setImei1(String imei1) {
		this.imei1 = imei1;
	}

	public String getImei2() {
		return imei2;
	}

	public void setImei2(String imei2) {
		this.imei2 = imei2;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPush_token() {
		return push_token;
	}

	public void setPush_token(String push_token) {
		this.push_token = push_token;
	}

	public String getDevice_uid1() {
		return device_uid1;
	}

	public void setDevice_uid1(String device_uid1) {
		this.device_uid1 = device_uid1;
	}

	public String getDevice_uid2() {
		return device_uid2;
	}

	public void setDevice_uid2(String device_uid2) {
		this.device_uid2 = device_uid2;
	}

	public String getOs_type() {
		return os_type;
	}

	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}

	public String getOs_version() {
		return os_version;
	}

	public void setOs_version(String os_version) {
		this.os_version = os_version;
	}

	public String getApp_version() {
		return app_version;
	}

	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}

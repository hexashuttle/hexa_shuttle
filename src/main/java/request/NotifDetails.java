package request;

public class NotifDetails {
	//sms 
	private String mobileNos;
	private String booking_sms_body;
	private String sms_url;
	private String sms_auth_key;
	private String sms_senderId;
	private String sms_routeId;
	private String smsContentType;
	private String cancel_booking_sms_body;
	
	//push 
	private String push_url;
	private String push_fcm_key;
	private String booking_push_body;
	private String cancel_booking_push_body;
	
	private String commonBody;
	
	
	

	
	
	public String getCommonBody() {
		return commonBody;
	}
	public void setCommonBody(String commonBody) {
		this.commonBody = commonBody;
	}
	public String getCancel_booking_sms_body() {
		return cancel_booking_sms_body;
	}
	public void setCancel_booking_sms_body(String cancel_booking_sms_body) {
		this.cancel_booking_sms_body = cancel_booking_sms_body;
	}
	public String getCancel_booking_push_body() {
		return cancel_booking_push_body;
	}
	public void setCancel_booking_push_body(String cancel_booking_push_body) {
		this.cancel_booking_push_body = cancel_booking_push_body;
	}
	public String getBooking_push_body() {
		return booking_push_body;
	}
	public void setBooking_push_body(String booking_push_body) {
		this.booking_push_body = booking_push_body;
	}
	public String getPush_url() {
		return push_url;
	}
	public void setPush_url(String push_url) {
		this.push_url = push_url;
	}
	public String getPush_fcm_key() {
		return push_fcm_key;
	}
	public void setPush_fcm_key(String push_fcm_key) {
		this.push_fcm_key = push_fcm_key;
	}
	public String getMobileNos() {
		return mobileNos;
	}
	public void setMobileNos(String mobileNos) {
		this.mobileNos = mobileNos;
	}
	public String getBooking_sms_body() {
		return booking_sms_body;
	}
	public void setBooking_sms_body(String booking_sms_body) {
		this.booking_sms_body = booking_sms_body;
	}
	public String getSms_url() {
		return sms_url;
	}
	public void setSms_url(String sms_url) {
		this.sms_url = sms_url;
	}
	public String getSms_auth_key() {
		return sms_auth_key;
	}
	public void setSms_auth_key(String sms_auth_key) {
		this.sms_auth_key = sms_auth_key;
	}
	public String getSms_senderId() {
		return sms_senderId;
	}
	public void setSms_senderId(String sms_senderId) {
		this.sms_senderId = sms_senderId;
	}
	public String getSms_routeId() {
		return sms_routeId;
	}
	public void setSms_routeId(String sms_routeId) {
		this.sms_routeId = sms_routeId;
	}
	public String getSmsContentType() {
		return smsContentType;
	}
	public void setSmsContentType(String smsContentType) {
		this.smsContentType = smsContentType;
	}
	
	
	

}

package request;

public class MarkReqest {
	private String user_id;
	private String passenger_id;
	private String email;

	private String route_id;
	private String route_timings_id;
	private String pick_stop_id;
	private String drop_stop_id;
	private String state;
	
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	public String getRoute_id() {
		return route_id;
	}
	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}
	public String getRoute_timings_id() {
		return route_timings_id;
	}
	public void setRoute_timings_id(String route_timings_id) {
		this.route_timings_id = route_timings_id;
	}
	public String getPick_stop_id() {
		return pick_stop_id;
	}
	public void setPick_stop_id(String pick_stop_id) {
		this.pick_stop_id = pick_stop_id;
	}
	public String getDrop_stop_id() {
		return drop_stop_id;
	}
	public void setDrop_stop_id(String drop_stop_id) {
		this.drop_stop_id = drop_stop_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	
}

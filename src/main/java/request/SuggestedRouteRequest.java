package request;

public class SuggestedRouteRequest {
	private String pick_loc;
	private String drop_loc;
	private String pick;
	private String drop;
	private String passenger_id;
	private String time;
	private String frequency;
	private String user_id;
	private String email;


	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getPick_loc() {
		return pick_loc;
	}
	public void setPick_loc(String pick_loc) {
		this.pick_loc = pick_loc;
	}
	public String getDrop_loc() {
		return drop_loc;
	}
	public void setDrop_loc(String drop_loc) {
		this.drop_loc = drop_loc;
	}
	public String getPick() {
		return pick;
	}
	public void setPick(String pick) {
		this.pick = pick;
	}
	public String getDrop() {
		return drop;
	}
	public void setDrop(String drop) {
		this.drop = drop;
	}
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	

}

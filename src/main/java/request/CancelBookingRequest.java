package request;

public class CancelBookingRequest {
	private String booking_id;
	private String passenger_id;
	private String trip_id;
	private String num_of_seat;
	private String email;
	private String booking_number;
	
	
	
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	public String getBooking_number() {
		return booking_number;
	}
	public void setBooking_number(String booking_number) {
		this.booking_number = booking_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNum_of_seat() {
		return num_of_seat;
	}
	public void setNum_of_seat(String num_of_seat) {
		this.num_of_seat = num_of_seat;
	}
	public String getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(String booking_id) {
		this.booking_id = booking_id;
	}
	public String getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

}

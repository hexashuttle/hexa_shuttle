package request;

public class COD_BookingRequest {

	private String passenger_id;
	//private String route_id;
	private String trip_id;
	private String pick_stop_id;
	private String 	drop_stop_id;
	private String number_of_seat;
	
	public String getPassenger_id() {
		return passenger_id;
	}
	public void setPassenger_id(String passenger_id) {
		this.passenger_id = passenger_id;
	}
	/**
	public String getRoute_id() {
		return route_id;
	}
	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}**/
	public String getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	public String getPick_stop_id() {
		return pick_stop_id;
	}
	public void setPick_stop_id(String pick_stop_id) {
		this.pick_stop_id = pick_stop_id;
	}
	public String getDrop_stop_id() {
		return drop_stop_id;
	}
	public void setDrop_stop_id(String drop_stop_id) {
		this.drop_stop_id = drop_stop_id;
	}
	public String getNumber_of_seat() {
		return number_of_seat;
	}
	public void setNumber_of_seat(String number_of_seat) {
		this.number_of_seat = number_of_seat;
	}
	



}

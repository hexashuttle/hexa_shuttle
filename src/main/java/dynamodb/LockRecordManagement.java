package dynamodb;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;

import config.ShuttleConfig;
import request.PersonRequest;

public class LockRecordManagement {
	
	private DynamoDB dynamoDB;
    //private String DYNAMODB_TABLE_NAME = "t_lock";
    private Regions REGION = Regions.AP_SOUTHEAST_1;
    private void initDynamoDbClient() {
    	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    	this.dynamoDB = new DynamoDB(client);
    }
	
	public PutItemOutcome createLockRecord() throws ConditionalCheckFailedException {
   	
		PutItemOutcome outcome = null;
		try {
			Table table = dynamoDB.getTable(ShuttleConfig.TABLE_LOCK);
			// Build the item
			Item item = new Item()
					.withPrimaryKey("id",String.valueOf(System.currentTimeMillis()) )
					.withString("firstName", "")
					.withString("lastName", "");
			// Write the item to the table 
			try {
				outcome = table.putItem(item);

			}catch(Exception e){
				System.err.println("Exception at while saving :"+e.getMessage());
			}
		}catch(ConditionalCheckFailedException ccfe){
			System.err.println("ConditionalCheckFailedException at createLockRecord :"+ccfe.getMessage());
		}catch(Exception e){
			System.err.println("Exception at createLockRecord :"+e.getMessage());

		}
		return outcome;
   }
}

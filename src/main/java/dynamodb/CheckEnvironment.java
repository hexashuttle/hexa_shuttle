package dynamodb;

public class CheckEnvironment {
	
	public static String getEnv(){
        String env = System.getenv("ENV");
        System.out.println("ENV : "+env);
        if(env==null || env.equals("") || env.equals("null")){
        	env="t";
        }
        System.out.println("Environment running on : "+env);
        return env;

    }

}

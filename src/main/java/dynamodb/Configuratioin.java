package dynamodb;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

import config.ShuttleConfig;

public class Configuratioin {
	
	public String getConfig(){

    	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
				.withRegion(Regions.AP_SOUTHEAST_1).build();
		
		DynamoDB dynamoDB = new DynamoDB(client);
		String  json_response=null;
		Table table = dynamoDB.getTable(ShuttleConfig.TABLE_CONFIG);
		GetItemSpec spec = new GetItemSpec().withPrimaryKey("id", 1);
		try {
			System.out.println("Attempting to read the item...");
		    Item outcome = table.getItem(spec);
		    System.out.println("outcome : "+outcome);
		    System.out.println("outcome.toString : "+outcome.toString());
		    //responseBody.put("config", outcome);
		    json_response=outcome.toJSON();
		            
		 }
		 catch (Exception e) {	           
		    System.err.println("Exception at ConfigHandler : "+e.getMessage());
		 }
		return json_response;
	}

}

package dynamodb;

import java.util.Iterator;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;

import config.ShuttleConfig;
import request.NotifDetails;

public class NotifConfig {
	
	
	public  NotifDetails getNotifConfig() {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
				.withRegion(Regions.AP_SOUTHEAST_1).build();
		NotifDetails notifDetObj=new NotifDetails();
		
		DynamoDB dynamoDB = new DynamoDB(client);
		Table table = dynamoDB.getTable(ShuttleConfig.TABLE_NOTIFICATION_CONFIG);
		GetItemSpec spec = new GetItemSpec().withPrimaryKey("id", 1);
		try {
			System.out.println("Attempting to read the item...");
		    Item outcome = table.getItem(spec);
		    notifDetObj.setBooking_sms_body(outcome.getString("booking_sms_body"));
		    notifDetObj.setSms_url(outcome.getString("sms_url"));
		    notifDetObj.setSms_auth_key(outcome.getString("sms_auth_key"));
		    notifDetObj.setSms_senderId(outcome.getString("sms_senderId"));
		    notifDetObj.setSms_routeId(outcome.getString("sms_routeId"));
		    notifDetObj.setSmsContentType(outcome.getString("sms_content_type"));
		    notifDetObj.setPush_url(outcome.getString("push_url"));
		    notifDetObj.setPush_fcm_key(outcome.getString("push_fcm_key"));
		    notifDetObj.setBooking_push_body(outcome.getString("booking_push_body"));
		    notifDetObj.setCancel_booking_sms_body(outcome.getString("cancel_booking_sms_body"));
		    notifDetObj.setCancel_booking_push_body(outcome.getString("cancel_booking_push_body"));
		    System.out.println("outcome : "+outcome);
		    System.out.println("GetItem succeeded: " + notifDetObj.getCancel_booking_push_body());
		            
		 }
		 catch (Exception e) {	           
		    System.err.println(e.getMessage());
		 }
		 System.out.println("notifDetObj "+notifDetObj.getBooking_sms_body());
		 return notifDetObj;
	}
	
	public Item getRawNotifConfig(){

    	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
				.withRegion(Regions.AP_SOUTHEAST_1).build();
		
		DynamoDB dynamoDB = new DynamoDB(client);
		Table table = dynamoDB.getTable(ShuttleConfig.TABLE_NOTIFICATION_CONFIG);
		GetItemSpec spec = new GetItemSpec().withPrimaryKey("id", 1);
		Item outcome=null;
		try {
			System.out.println("Attempting to read the item...");
		    outcome = table.getItem(spec);
		    System.out.println("outcome : "+outcome);
		    System.out.println("outcome.driver_push_body : "+outcome.getString("driver_push_body"));
		    //responseBody.put("config", outcome);
		    //json_response=outcome.toJSON();
		            
		 }
		 catch (Exception e) {	           
		    System.err.println("Exception at ConfigHandler : "+e.getMessage());
		 }
		return outcome;
	}
}

package mongo;

import java.net.UnknownHostException;

import org.bson.Document;

import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import config.AppConfig;

/**
 * 
 * @author satya
 * mongodb connector
 */
public class MongoConnector {
	
	//private static final ALogger ilogger = Logger.of(MongoConnector.class);
	public static MongoDatabase database=null;
	public static MongoClient mongoClient=null;
	
	public static MongoCollection<Document> getConnection(String collectionName){

        @SuppressWarnings("unused")
		MongoCollection<Document> collection=null;
        //MongoDatabase database=null;
        try {
        	/**
        	MongoClientOptions.Builder options_builder = new MongoClientOptions.Builder();
            options_builder.maxConnectionIdleTime(60000);
            MongoClientOptions options = options_builder.build();
            **/
            MongoClientURI connectionString = new MongoClientURI(AppConfig.MONGO_CLIENT_URL);
            
            if(mongoClient==null){
            	mongoClient = new MongoClient(connectionString);
			}

            // Now connect to your databases
            if(database==null){
            	database = mongoClient.getDatabase(AppConfig.MONGO_DB);
              }
            //ilogger.info("Connect to database:"+AppConfig.MONGO_DB+" successfully");
            collection=database.getCollection(collectionName);
            System.out.println("Connect to db: "+AppConfig.MONGO_DB+" and collection: "+collectionName+" successfully");
        }catch(MongoSocketOpenException e){
        	System.err.println( "MongoSocketOpenException ="+e.getClass().getName() + ": " + e.getMessage() );
        }catch(MongoTimeoutException e){
            System.err.println( "MongoTimeoutException ="+e.getClass().getName() + ": " + e.getMessage() );
        }catch(Exception e){
            System.err.println( "Exception at getConnection="+e.getClass().getName() + ": " + e.getMessage() );
        }
        return collection;
    }
}

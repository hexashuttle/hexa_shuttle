package mongo;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.json.simple.JSONObject;

import utils.CustomValidator;
import com.mongodb.client.model.Projections;
import config.AppConfig;
import static com.mongodb.client.model.Filters.*;

import java.util.List;


/**
 * 
 * @author satya
 * via point serving api
 */
public class DashBoard {
	
	public String getViaPoint(String email,String student_id){
    	System.out.println("Dashboard api called");
	    //String email = "satya_driver@hexa.com";
		String response="{\"driver_status\":\"99\"}";
		Document riders=null;
		Document driver=null;
		//JSONObject riders_obj = new JSONObject();
		//JSONObject driver_obj = new JSONObject();
		if (email != null) {
			if(student_id!=null && !student_id.equals("")) {
				System.out.println("student_id="+student_id);
				boolean owner_side=CustomValidator.isEmailValid(student_id);
				System.out.println("Email validation="+owner_side);
				if(!owner_side){
					System.out.println("Parent Dashboard api.");
			    	MongoCollection<Document> collection=null;
			    	try {
			            collection = MongoConnector.getConnection(AppConfig.RIDERS_COLLECTION_NAME);
			            FindIterable<Document> cursor = collection.find(
			            			and(eq("email",email),
			            				eq("student_id",Integer.parseInt(student_id))/*,
			            				ne("student_status",Integer.parseInt(AppConfig.ABSENT_BY_DRIVER)),
			            				ne("student_status",Integer.parseInt(AppConfig.ABSENT_BY_PARENT))*/
			            				)).projection(Projections.exclude("_id"));
			            int cursor_size=0;
			            for (Document cur : cursor) {
			                cursor_size=cursor_size+1;
			                riders=cur;
			            }
			            System.out.println("cursor_size for rider="+cursor_size);
			        } catch(Exception e) {
			        	System.err.println("Exception at DashBoard for rider read="+e.getMessage());
			        }
			    	if(riders!=null && riders.size()>0) {
						String driver_email=riders.getString("driver_email");
						
						/**
						 * Added for rfid vehicle
						 */
						RfidDriverFinder obj=new RfidDriverFinder();
						boolean result=obj.isRfidDriver(driver_email);
						System.out.println("driver_email="+driver_email);
						if(driver_email!=null && !driver_email.equals("")){
							try {
								MongoCollection<Document> drive_col=MongoConnector.getConnection(AppConfig.DRIVER_COLLECTION_NAME);
								FindIterable<Document> cursor = drive_col.find(eq("email",driver_email)).projection(Projections.exclude("_id"));
								for (Document cur : cursor) {
									driver=cur;
								}
							 } catch(Exception e) {
								 System.err.println("Exception at DashBoard for driver read="+e.getMessage());
						     }
						}
						if(driver!=null && driver.size()>0) {
							riders.append("driver_mobile", driver.get("mobile"))
							  	.append("driver_name", driver.get("name"))
							  	.append("driver_image", driver.get("image"))
							  	.append("speed", driver.get("speed"))
							  	.append("driver_lat", driver.get("latitude"))
							  	.append("driver_lon", driver.get("longitude"))
							  	.append("bearing", driver.get("bearing"))
							  	.append("license_plate", driver.get("license_plate"));
							if(result){
								System.out.println("This is rfid driver : "+driver_email);
								riders.append("dest_lat", driver.get("latitude"))
							  		.append("dest_lon", driver.get("longitude"));
							}
						}
						//return ok(riders.toJson());
					}
			    	//return ok(response);
				} else {
					System.out.println("Owner Dashboard api.");
					try {
						MongoCollection<Document> drive_col=MongoConnector.getConnection(AppConfig.DRIVER_COLLECTION_NAME);
						FindIterable<Document> cursor = drive_col.find(eq("email",student_id)).projection(Projections.exclude("_id"));
						for (Document cur : cursor) {
							driver=cur;
						}
						System.out.println("driver size="+driver.size());
						if(driver!=null && driver.size()>0) {
							PartnerViaPoint pObj=new PartnerViaPoint();
							List<Document> vList=pObj.getTripDetails(driver);
							System.out.println("vList size="+vList.size());
							driver.append("via_points", vList);
							//return ok(driver.toJson());
						} else {
							//return ok(response);
						}
					 } catch(Exception e) {
						 System.err.println("Exception at DashBoard for driver read="+e.getMessage());
				     }
					 //return ok(response);
				}
		} else {
		    	System.out.println("student_id is null or blank");
	    		//return badRequest("student_id is null or balnk");
	    	}
		}
		System.out.println("Json Response : "+riders.toJson());
		

		
		return riders.toJson();
    }
	
}
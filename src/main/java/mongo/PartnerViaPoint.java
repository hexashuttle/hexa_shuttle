package mongo;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

import config.AppConfig;


public class PartnerViaPoint {
	//private static final ALogger ilogger = Logger.of(PartnerViaPoint.class);
	
	public List<Document> getTripDetails(Document driver){
		List<Document> vList=new ArrayList<Document>();
		int driver_status=0;
		try{
			//ilogger.debug("getTripDetails="+driver.getString("email")+" shift_id="+driver.getInteger("shift_id"));
			
			MongoCollection<Document> riderCollection=MongoConnector.getConnection(AppConfig.RIDERS_COLLECTION_NAME);
			FindIterable<Document> cursor = riderCollection.find(
	    			and(eq("driver_email",driver.getString("email")),
	    				eq("shift_id",driver.getInteger("shift_id")),
	    				nin("student_status",Integer.parseInt(AppConfig.ABSENT_BY_PARENT),
	    						Integer.parseInt(AppConfig.ABSENT_BY_DRIVER))
	    				)).sort(Sorts.ascending("order_id"))
					      .projection(Projections.exclude("_id"));
			 int cursor_size=0;
	         for (Document cur : cursor) {
	        	 if(cursor_size==0){
	        		System.out.println("cur.getInteger(driver_status)"+cur.getInteger("driver_status")); 
	        		driver.put("driver_status",cur.getInteger("driver_status"));
	        	 }
	        	 if(cur.getInteger("driver_status")==Integer.parseInt(AppConfig.READY_TO_PICKUP_FROM_HOME)){
	        		 if(cur.getInteger("student_status")== Integer.parseInt(AppConfig.READY_TO_PICKUP_FROM_HOME)){
	        			 Document via_points=new Document();
	        			 via_points.put("rider_id", cur.getInteger("rider_id"));
			             via_points.put("loc_lat", cur.getDouble("pickUp_lat"));
			             via_points.put("loc_lon", cur.getDouble("pickUp_lon"));
			             
			             via_points.put("student_status", cur.getInteger("student_status"));
			             via_points.put("order_id", cur.getInteger("order_id"));
			             via_points.put("shift_id", cur.getInteger("shift_id"));
			             via_points.put("student_name", cur.get("student_name"));
			             via_points.put("student_image", cur.get("student_image"));
			             vList.add(via_points);
			             driver.put("dest_lat", cur.getDouble("school_lat"));
	        			 driver.put("dest_lon", cur.getDouble("school_lon"));
	        		 }
	        	 }else if(cur.getInteger("driver_status")==Integer.parseInt(AppConfig.ONRIDE_FROM_HOME)){
	        		driver.put("dest_lat", cur.getDouble("school_lat"));
	        		driver.put("dest_lon", cur.getDouble("school_lon"));
	        		
	        	 }else if(cur.getInteger("driver_status")==Integer.parseInt(AppConfig.READY_TO_PICKUP_FROM_SCHOOL)){
//	        		 if(cursor_size==0){
//	        			 driver.put("driver_status",cur.getInteger("driver_status"));
//	        		 }
	        	 }else if(cur.getInteger("driver_status")==Integer.parseInt(AppConfig.ONRIDE_FROM_SCHOOL)){
	        		 driver_status=Integer.parseInt(AppConfig.ONRIDE_FROM_SCHOOL);
	        		 if(cur.getInteger("student_status")==Integer.parseInt(AppConfig.ONRIDE_FROM_SCHOOL)){
	        			 Document via_points=new Document();
	        			 via_points.put("rider_id", cur.getInteger("rider_id"));
			             via_points.put("loc_lat", cur.getDouble("pickUp_lat"));
			             via_points.put("loc_lon", cur.getDouble("pickUp_lon"));
			             
			             via_points.put("student_status", cur.getInteger("student_status"));
			             via_points.put("order_id", cur.getInteger("order_id"));
			             via_points.put("shift_id", cur.getInteger("shift_id"));
			             via_points.put("student_name", cur.get("student_name"));
			             via_points.put("student_image", cur.get("student_image"));
			             vList.add(via_points);
			           
	        		 }
	        	}else if(cur.getInteger("driver_status")==AppConfig.DROP_ALL_AT_HOME){
//	        		 if(cursor_size==0){
//	        			 driver.put("driver_status",cur.getInteger("driver_status"));
//	        		 }
	        	 }
	        	 cursor_size=cursor_size+1; 
	         }
	         if(driver_status==Integer.parseInt(AppConfig.ONRIDE_FROM_SCHOOL)){
	        	 Collections.reverse(vList); 
	         }
	         System.out.println("cursor_size ="+cursor_size+" via_points="+vList.size());
		}catch(MongoException e){
			System.err.println( "MongoException getTripDetails="+e.getClass().getName() + ": " + e.getMessage() );
        }catch(Exception e){
        	System.err.println( "Exception at getTripDetails="+e.getClass().getName() + ": " + e.getMessage() );
        }
		
         return vList;
	}
}

package controller;

import java.util.Map;

public class TakeDecision {
	
	public boolean isBookingAllowed(Map<String,String> seatMap){
		boolean bookingAllowed=false;
		if(seatMap!=null && seatMap.size()>0){
			int total_seats=Integer.parseInt(seatMap.get("total_seats"));
			int remaining_seats=Integer.parseInt(seatMap.get("remaining_seats"));
			if(total_seats>=remaining_seats && remaining_seats>0){
				bookingAllowed=true;
			}else{
				System.out.println("All seat booked.");
			}
		}else{
			System.out.println("seatMap is null or empty");
		}
		System.out.println("Seat booking allowed : "+bookingAllowed);
		return bookingAllowed;
	}
}

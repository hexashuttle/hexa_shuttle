package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import db.operartion.Helper;
import lock.LockPassenger;
import request.BookingRequest;
import utils.SeatMapper;

public class InitiatePay {
	
	@SuppressWarnings("unused")
	public Map<String,String> initatePaymentProcess(BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj){
		
		 Helper helperObj=new Helper();
		 Map<String,String> bookingMap=new HashMap<String,String>();

	     Map<String,String> availableSeatMap=helperObj.getAvailableSeat(bookingObj, dataSource, connObj);
	     TakeDecision decisionObj=new TakeDecision();
	     boolean isBookingAllowed=decisionObj.isBookingAllowed(availableSeatMap);
	     if(isBookingAllowed){
	    	 SeatMapper setMapObj=new SeatMapper();
	         List<String> vacantSeatList=setMapObj.getSeatList(availableSeatMap);
	         LockPassenger lockObj=new LockPassenger();
	    	 try {
	    		 for(int i=0;i<vacantSeatList.size();i++){
	    			 if(isBookingAllowed){
	    				 System.out.println("Try to lock for key : "+vacantSeatList.get(i));
	    				 bookingMap=lockObj.lockingWhilePaymentInitiation(vacantSeatList.get(i),bookingObj,dataSource,connObj);
	        	    	 //Break the loop when lock created
	    				 boolean lock_created=Boolean.parseBoolean(bookingMap.get("lock_exits"));
	    				 System.out.println("lock_created : "+bookingMap.get("lock_exits")+" lock_created :"+lock_created);

	        	    	 if(lock_created){
	        	    		 System.out.println("Payment Done!!!");
	        	    		break;
	        	    	 }
	    	    	 }
	    	     }
	    	 } catch (InterruptedException | IOException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    			System.out.println("Exception at initatePaymentProcess : "+e.getMessage());
	    	 }
	      }
	        System.out.println("bookingMap in initatePaymentProcess : "+bookingMap);
	     return bookingMap;
		}
}

package controller;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import db.operartion.CancelBooking;
import db.operartion.Helper;
import db.operartion.PickUpByPassenger;
import request.CancelBookingRequest;
import request.PickUpByPassengerRequest;

public class PickUpByPassengerProcessor {
	
	@SuppressWarnings("unused")
	public Map<String,String> pickUpByPassenger(PickUpByPassengerRequest pickUpPassReqObj,
			DataSource dataSource,
			Connection connObj){
		Map<String,String> resMap=new HashMap<String,String>();
		PickUpByPassenger pickUpByPassObj=new PickUpByPassenger();
		Helper helpObj=new Helper();
		resMap=pickUpByPassObj.pickUp(pickUpPassReqObj, dataSource, connObj);
		if(resMap!=null && !resMap.isEmpty()){
			if(resMap.containsKey("statusCode")){
				int statusCode=Integer.parseInt(resMap.get("statusCode"));
				System.out.println("PickUpByPassengerProcessor statusCode : "+statusCode);
				if(statusCode==200){
					resMap.put("trip_id", pickUpPassReqObj.getTrip_id());
					String passenger_name=helpObj.getPassengerNameFromPassengerID(pickUpPassReqObj.getPassenger_id(),dataSource,connObj);
					if(passenger_name!=null && !passenger_name.equals("")){
						resMap.put("name", passenger_name);
						resMap.put("email", pickUpPassReqObj.getEmail());
						SendNotif sendNotifObj=new SendNotif();
		            	sendNotifObj.invokePassengerBookingNotification(resMap, dataSource, connObj);
					}
				}
			}
		}else{
        	resMap.put("message", "PickUp by passenger failed.");
    		resMap.put("statusCode", "500");
        }
		return resMap;
	}

}

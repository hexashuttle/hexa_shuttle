package controller;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import db.operartion.CancelBooking;
import request.CancelBookingRequest;

public class CancelBookingProcessor {
	
	@SuppressWarnings("unused")
	public Map<String,String> canBookingProcessor(CancelBookingRequest cancelBookReqObj,
			DataSource dataSource,
			Connection connObj){
		
		Map<String,String> resMap=new HashMap<String,String>();
		CancelBooking canBookingObj=new CancelBooking();
		resMap=canBookingObj.canBookSeat(cancelBookReqObj, dataSource, connObj);
		if(resMap!=null && !resMap.isEmpty()){
			if(resMap.containsKey("statusCode")){
				int statusCode=Integer.parseInt(resMap.get("statusCode"));
				System.out.println("CancelBookingProcessor statusCode : "+statusCode);
				if(statusCode==200){
					SendNotif sendNotifObj=new SendNotif();
					resMap.put("booking_number", cancelBookReqObj.getBooking_number());
	            	sendNotifObj.invokeCancelBookinNotification(resMap,cancelBookReqObj.getEmail(), dataSource, connObj);
				}
			}
		}else{
        	resMap.put("message", "Booking Cancelled failed.");
    		resMap.put("statusCode", "500");
        }
		return resMap;
	}

}

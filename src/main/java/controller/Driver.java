package controller;

import config.AppConfig;
import rds.ConnectionPool;
import utils.*;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Driver {
	
    public String getMobileNumber(String user_id){

        ResultSet rsObj = null;
        Connection connObj = null;
        PreparedStatement pstmtObj = null;
        ConnectionPool jdbcObj = new ConnectionPool();
        String mobile=null;
        try {
            DataSource dataSource = jdbcObj.setUpPool();
            jdbcObj.printDbStatus();

            // Performing Database Operation!
            System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
                connObj = dataSource.getConnection();
                jdbcObj.printDbStatus();
        	}
            

            String getAttendantMobileNumberQuery="select mobile from api_hexauser where id="+user_id+" and role='Driver' ";
            System.out.println("getAttendantMobileNumber Query = "+getAttendantMobileNumberQuery);

            pstmtObj = connObj.prepareStatement(getAttendantMobileNumberQuery);

            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {

                mobile=rsObj.getString("mobile");
                System.out.println("mobile : " + rsObj.getString("mobile"));
            }
            System.out.println("\n=====Releasing Connection Object To Pool=====\n");
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getAttendantMobileNumber "+sqlException.getMessage());
            sqlException.printStackTrace();
        } finally {
            try {
                // Closing ResultSet Object
                if(rsObj != null) {
                    rsObj.close();
                }
                // Closing PreparedStatement Object
                if(pstmtObj != null) {
                    pstmtObj.close();
                }
                // Closing Connection Object
                if(connObj != null) {
                    connObj.close();
                }
            } catch(Exception sqlException) {
                System.out.println("Exception when closing at: getMobileNumber "+sqlException.getMessage());
                sqlException.printStackTrace();
            }
        }
        jdbcObj.printDbStatus();
        return mobile;
    }

}

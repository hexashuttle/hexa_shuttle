package controller;

import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;

import config.ShuttleConfig;
import db.operartion.Helper;
import db.operartion.SaveNoitification;
import dynamodb.NotifConfig;
import notif.InvokePush;
import notif.InvokeSms;
import request.NotifDetails;
import utils.NotificationBody;
import utils.UserConfigMapper;

public class SendNotif {
	
	public void invokeBookinNotification(Map<String,String> resMap,
			String email,
			DataSource dataSource,
			Connection connObj){
		Helper helpObj=new Helper();
		
		System.out.println("Start reading notification configuration.");
		UserConfigMapper userConfMapObj=helpObj.getUserSettings(email, dataSource, connObj);
		boolean is_sms=userConfMapObj.is_sms;
		boolean is_push=userConfMapObj.is_push;
		System.out.println("Action settings for is_sms : "+is_sms+" is_push : "+is_push);
		if(is_sms){
			String user_mobile=helpObj.getUserMobile(email, dataSource, connObj);
	    	System.out.println("Mobile : "+user_mobile);
	    	if(user_mobile!=null && !user_mobile.equals("")){
	    		NotifConfig notifConfigObj=new NotifConfig();
	 		    NotifDetails notifDetailObj= notifConfigObj.getNotifConfig();
	 		    notifDetailObj.setMobileNos(user_mobile);
	 		    notifDetailObj.setCommonBody(notifDetailObj.getBooking_sms_body());
	 		    boolean result=false;
	        	try{
	        		//Insert sms in shuttle system
	        		String body=NotificationBody.getBookingNotifBodyForDb(resMap, notifDetailObj.getBooking_sms_body());
	        		notifDetailObj.setCommonBody(NotificationBody.getBookingNotifBodyForMsg(resMap, notifDetailObj.getBooking_sms_body()));
	        		SaveNoitification saveNotifObj=new SaveNoitification();
	 	 		   	result=saveNotifObj.insertSms(email, 
	 	 		   			body, 
	 	 				   ShuttleConfig.BOOKING_CONFIRMED, 
	 	 				   ShuttleConfig.IS_SMS, 
	 	 				   dataSource, 
	 	 				   connObj);
	        	}catch(Exception ex){
	        		System.out.println("Exception at sms insert : "+ex.getMessage());
	        	}
	        	System.out.println("SMS Insert notification result : "+result);
	 		   if(result){
	 			  System.out.println("Invoke sms action.");
				  InvokeSms invokeSmsObj=new InvokeSms();
	   		      invokeSmsObj.sendSms(notifDetailObj);
	 		   }
	    	}else{
	    		System.out.println("Mobile not found for user : "+email);
	    	}
 		}
		if(is_push){
 		   String push_token=helpObj.getUserPushToken(email, dataSource, connObj);
 		   System.out.println("push_token : "+push_token);
 		  if(push_token!=null && !push_token.equals("")){
 			  NotifConfig notifConfigObj=new NotifConfig();
	 		  NotifDetails notifDetailObj= notifConfigObj.getNotifConfig();
	 		  notifDetailObj.setCommonBody(notifDetailObj.getBooking_push_body());
	 		 String push_body=NotificationBody.getBookingNotifBodyForDb(resMap, notifDetailObj.getBooking_push_body());
     		 notifDetailObj.setCommonBody(NotificationBody.getBookingNotifBodyForMsg(resMap, notifDetailObj.getBooking_push_body()));
     		
	 		    boolean result=false;
	 		   try{
	 	 		  //Insert push in shuttle system
	 			   SaveNoitification saveNotifObj=new SaveNoitification();
	 	 		  result=saveNotifObj.insertPush(email, 
	 	 				  push_body, 
	 	 				   ShuttleConfig.BOOKING_CONFIRMED, 
	 	 				   ShuttleConfig.IS_PUSH, 
	 	 				   dataSource, 
	 	 				   connObj);
	        	}catch(Exception ex){
	        		System.out.println("Exception at sms insertPush : "+ex.getMessage());
	        	}
	 		  System.out.println("Push Insert notification result : "+result);
	 		  if(result){
	 			  System.out.println("Invoke push action.");
	 			 InvokePush invokePushObj=new InvokePush();
	 			 invokePushObj.pushMessage(notifDetailObj, push_token,ShuttleConfig.BOOKING_ACTION);
	 		   }
	    	}else{
	    		System.out.println("push_token not found for user : "+email);
	    	}
		}
	}
	
	public void invokeCancelBookinNotification(Map<String,String> resMap,
			String email,
			DataSource dataSource,
			Connection connObj){

		Helper helpObj=new Helper();
		
		System.out.println("Start reading notification configuration.");
		UserConfigMapper userConfMapObj=helpObj.getUserSettings(email, dataSource, connObj);
		boolean is_sms=userConfMapObj.is_sms;
		boolean is_push=userConfMapObj.is_push;
		System.out.println("Action settings for is_sms : "+is_sms+" is_push : "+is_push);
		if(is_sms){
			String user_mobile=helpObj.getUserMobile(email, dataSource, connObj);
	    	System.out.println("Mobile : "+user_mobile);
	    	if(user_mobile!=null && !user_mobile.equals("")){
	    		NotifConfig notifConfigObj=new NotifConfig();
	 		    NotifDetails notifDetailObj= notifConfigObj.getNotifConfig();
	 		    notifDetailObj.setMobileNos(user_mobile);
        		String body=NotificationBody.getCancelBookingNotifBodyForDb(resMap, notifDetailObj.getCancel_booking_sms_body());
        		notifDetailObj.setCommonBody(NotificationBody.getCancelBookingNotifBodyForMsg(resMap, notifDetailObj.getCancel_booking_sms_body()));

	 		    //Insert sms in shuttle system
	 		   SaveNoitification saveNotifObj=new SaveNoitification();
	 		   boolean result=saveNotifObj.insertSms(email, 
	 				   body, 
	 				   ShuttleConfig.BOOKING_CANCEL, 
	 				   ShuttleConfig.IS_SMS, 
	 				   dataSource, 
	 				   connObj);
	 		   System.out.println("SMS Insert notification result : "+result);
	 		   if(result){
	 			  System.out.println("Invoke sms action.");
				  InvokeSms invokeSmsObj=new InvokeSms();
	   		      invokeSmsObj.sendSms(notifDetailObj);
	 		   }
	    	}else{
	    		System.out.println("Mobile not found for user : "+email);
	    	}
 		}
		if(is_push){
 		   String push_token=helpObj.getUserPushToken(email, dataSource, connObj);
 		   System.out.println("push_token : "+push_token);
 		  if(push_token!=null && !push_token.equals("")){
 			  NotifConfig notifConfigObj=new NotifConfig();
	 		  NotifDetails notifDetailObj= notifConfigObj.getNotifConfig();
	 		  String push_body=NotificationBody.getCancelBookingNotifBodyForDb(resMap, notifDetailObj.getCancel_booking_push_body());
	     	  notifDetailObj.setCommonBody(NotificationBody.getCancelBookingNotifBodyForMsg(resMap, notifDetailObj.getCancel_booking_push_body()));
	     		
	 		  //Insert push in shuttle system
	 		  SaveNoitification saveNotifObj=new SaveNoitification();
	 		  boolean result=saveNotifObj.insertPush(email, 
	 				 push_body, 
	 				   ShuttleConfig.BOOKING_CANCEL, 
	 				   ShuttleConfig.IS_PUSH, 
	 				   dataSource, 
	 				   connObj);
	 		  System.out.println("Push Insert notification result : "+result);
	 		  if(result){
	 			  System.out.println("Invoke push action.");
	 			 InvokePush invokePushObj=new InvokePush();
	 			 invokePushObj.pushMessage(notifDetailObj, push_token,ShuttleConfig.CANCEL_BOOKING_ACTION);
	 		   }
	    	}else{
	    		System.out.println("push_token not found for user : "+email);
	    	}
		}
	
		
	}
	
	public void invokePassengerBookingNotification(Map<String,String> resMap,
			DataSource dataSource,
			Connection connObj){
		Helper helpObj=new Helper();
		String driver_push_token=helpObj.getDriverPushTokenFromTrip(resMap.get("trip_id"), dataSource, connObj);
		System.out.println("driver_push_token : "+driver_push_token);
		if(driver_push_token!=null && !driver_push_token.equals("")){
			NotifConfig notifConfigObj=new NotifConfig();
			String push_body=notifConfigObj.getRawNotifConfig().getString("driver_push_body");
			String push_title="PASSENGER PICKUP";
			String action="PP";
			String push_fcm_key=notifConfigObj.getRawNotifConfig().getString("push_fcm_key");
			String name=resMap.get("name");
			String email=resMap.get("email");
			String push_url=notifConfigObj.getRawNotifConfig().getString("push_url");

			
			//Insert push to db
			SaveNoitification saveNotifObj=new SaveNoitification();
	 		boolean result=saveNotifObj.insertPush(email, 
	 				  push_body, 
	 				   ShuttleConfig.PASSENGER_PICK_PUSH_CODE, 
	 				   ShuttleConfig.IS_PUSH, 
	 				   dataSource, 
	 				   connObj);
	 		if(result){
	 			 System.out.println("Invoke push action.");
	 			 InvokePush invokePushObj=new InvokePush();
	 			 invokePushObj.pushMessageToDriver(push_body,
	 					push_title,
	 					action,
	 					push_fcm_key,
	 					name,
	 					push_url,
	 					driver_push_token);  
	 		}

		}else{
    		System.out.println("push_token not found for driver of trip_id : "+resMap.get("trip_id"));
    	}
		
	}
	
	
}

package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import db.operartion.Helper;
import lock.LockPassenger;
import request.BookingRequest;
import utils.SeatMapper;

public class BookingProccessor {
	
	@SuppressWarnings("unused")
	public Map<String,String> crateBooking(BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj){
		
		String result="";
		String message="";
		Map<String,String> resMap=new HashMap<String,String>();
		Helper helperObj=new Helper();
	    Map<String,String> availableSeatMap=helperObj.getAvailableSeat(bookingObj, dataSource, connObj);
	    TakeDecision decisionObj=new TakeDecision();
	    boolean isBookingAllowed=decisionObj.isBookingAllowed(availableSeatMap);
	    if(isBookingAllowed){
	    	 SeatMapper setMapObj=new SeatMapper();
	         List<String> vacantSeatList=setMapObj.getSeatList(availableSeatMap);
	         LockPassenger lockObj=new LockPassenger();
	    	 try {
	    		 for(int i=0;i<vacantSeatList.size();i++){
	    			 if(isBookingAllowed){
	    				 if(!resMap.isEmpty()){
	    					 resMap.clear();
	    				 }
	    				 System.out.println("Try to lock for key : "+vacantSeatList.get(i));
	    				 Map<String,String> lockProcessMap=lockObj.createLockAndBookSeat(vacantSeatList.get(i),bookingObj,dataSource,connObj);
	    				 System.out.println("lockProcessMap : "+lockProcessMap);
	        	    	 //Break the loop when lock created
	    				 if(lockProcessMap!=null && !lockProcessMap.isEmpty()){
		    				 result=lockProcessMap.get("result");
	    				 }
	    				 System.out.println("result : "+result);
	        	    	 if(result.equals("success")){
	        	    		if(lockProcessMap!=null && lockProcessMap.containsKey("booking_id")){
	        	    			resMap.put("booking_id", lockProcessMap.get("booking_id"));
	        	    		}
	        	    		if(lockProcessMap!=null && lockProcessMap.containsKey("booking_number")){
	        	    			resMap.put("booking_number", lockProcessMap.get("booking_number"));
	        	    		}
	        	    		if(lockProcessMap!=null && lockProcessMap.containsKey("license_plate")){
	        	    			resMap.put("license_plate", lockProcessMap.get("license_plate"));
	        	    		}
	        	    		if(lockProcessMap!=null && lockProcessMap.containsKey("stop_name")){
	        	    			resMap.put("stop_name", lockProcessMap.get("stop_name"));
	        	    		}
	        	    		resMap.put("message", "Booking sucessfull.");
	        	    		resMap.put("statusCode", "200");
	        	    		break;
	        	    	 }else{
	        	    		resMap.put("message", "Booking failed.");
		        	    	resMap.put("statusCode", "500");
	        	    	 }
	    	    	 }
	    	     }
	    	 } catch (InterruptedException | IOException e) {
	    			// TODO Auto-generated catch block
	    			System.out.println("Exception at initatePaymentProcess : "+e.getMessage());
	    			resMap.put("message", "Internal Server Error");
	    			resMap.put("statusCode", "500");
	    			
	    	 }
	      }else{
	    	resMap.put("message", "No seat available for booking.");
  	    	resMap.put("statusCode", "400");
	      }
	    System.out.println("resMap : "+resMap);
	    return resMap;
		}

}

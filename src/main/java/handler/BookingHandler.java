package handler;


import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import controller.TakeDecision;
import rds.ConnectionPool;
import request.BookingRequest;
import utils.SeatMapper;
import db.operartion.Helper;
import lock.LockPassenger;

public class BookingHandler implements RequestHandler<BookingRequest, Object> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public BookingHandler() throws Exception {
		System.out.println("\n HandlerBooking Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }

    @SuppressWarnings("unchecked")
	@Override
    public JSONObject handleRequest(BookingRequest bookingObj, Context context) {
        context.getLogger().log("bookingObj: " + bookingObj);
        System.out.println("\n conObj = "+connObj);
        JSONObject responseBody = new JSONObject();
        responseBody.put("passenger_id", bookingObj.getPassenger_id());
        
        Helper helperObj=new Helper();
        Map<String,String> availableSeatMap=helperObj.getAvailableSeat(bookingObj, dataSource, connObj);
        TakeDecision decisionObj=new TakeDecision();
        boolean isBookingAllowed=decisionObj.isBookingAllowed(availableSeatMap);
        if(isBookingAllowed){
        	SeatMapper setMapObj=new SeatMapper();
        	List<String> vacantSeatList=setMapObj.getSeatList(availableSeatMap);
        	LockPassenger lockObj=new LockPassenger();
    	    try {
    	    	for(int i=0;i<vacantSeatList.size();i++){
    	    		if(isBookingAllowed){
    	    			System.out.println("Try to lock for key : "+vacantSeatList.get(i));
        	    		boolean lock_created=lockObj.lockingWhileSeatConfirmation(vacantSeatList.get(i),bookingObj,dataSource,connObj);
        	    		//Break the loop when lock created
        	    		if(lock_created){
        	    			break;
        	    		}
    	    		}
    	    	}
    	    } catch (InterruptedException | IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
        responseBody.put("result",true );
        return responseBody;
    }
}

package handler;


import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import controller.InitiatePay;
import controller.PaytmChecksumProcessor;
import notif.InvokeSms;
import rds.ConnectionPool;
import request.BookingRequest;

public class InitiatePaymentHandler implements RequestHandler<BookingRequest, Object> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public InitiatePaymentHandler() throws Exception {
		System.out.println("\n InitiatePaymentHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }

    @SuppressWarnings("unchecked")
	@Override
    public JSONObject handleRequest(BookingRequest bookingObj, Context context) {
        context.getLogger().log("bookingObj: " + bookingObj);
        System.out.println("\n conObj = "+connObj);
        JSONObject responseBody = new JSONObject();
        responseBody.put("passenger_id", bookingObj.getPassenger_id());
		Map<String,String> bookingMap=new HashMap<String,String>();

        InitiatePay initiatePayObj=new InitiatePay();
        bookingMap=initiatePayObj.initatePaymentProcess(bookingObj, dataSource, connObj);
        System.out.println("bookingMap in InitiatePaymentHandler : "+bookingMap);
        if(bookingMap!=null && !bookingMap.isEmpty() && bookingMap.containsKey("booking_id")){
        	//Initiate Payment Process
        	System.out.println("Execute Pay Process");
        	PaytmChecksumProcessor payObj=new PaytmChecksumProcessor();
        	//responseBody=payObj.paytmCheckSumGenerator(bookingObj, dataSource, connObj,bookingMap);
        	
        }
        
        //responseBody.put("result",true );
        return responseBody;
    }
}

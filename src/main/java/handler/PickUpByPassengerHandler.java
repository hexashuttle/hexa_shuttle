package handler;//PickUpByPassengerHandler

import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import controller.PickUpByPassengerProcessor;
import db.operartion.Helper;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;
import rds.ConnectionPool;
import request.BookingRequest;
import request.PickUpByPassengerRequest;
import utils.AwsReqParserToPojo;

public class PickUpByPassengerHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public PickUpByPassengerHandler() throws Exception {
		System.out.println("\n PickUpByPassengerHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {
		System.out.println("\n PickUpByPassengerHandler handleRequest called..");

		String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
	    System.out.println("Email : "+email);
	    AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
	    JSONObject responseBody = new JSONObject();
	    if(email!=null && !email.equals("")){
	    	int statusCode=500;
	    	Helper helperObj=new Helper();
	    	AwsReqParserToPojo awsReqParserObj=new AwsReqParserToPojo();
	    	PickUpByPassengerRequest pickUpPassReqObj= awsReqParserObj.getPickUpByPassengerRequest(awsProxyReqObj.getBody());
	    	pickUpPassReqObj.setPassenger_id(helperObj.getPassengerIdFromUserEmail(email, dataSource, connObj));
	    	pickUpPassReqObj.setTrip_id(helperObj.getTripFromBookingId(pickUpPassReqObj.getBooking_id(), dataSource, connObj));
	    	
	    	PickUpByPassengerProcessor processorObj=new PickUpByPassengerProcessor();
	    	Map<String,String> resMap=processorObj.pickUpByPassenger(pickUpPassReqObj, dataSource, connObj);
	    	if(resMap!=null && !resMap.isEmpty()){
                responseBody.put("message",resMap.get("message") );
                statusCode=Integer.parseInt(resMap.get("statusCode"));
            }else{
            	responseBody.put("message","PickUp by passenger failed." ); 
            }
	    	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(statusCode);
	    }else {
	        responseBody.put("message", "Unauthorized");
	        awsProxyResObj.setBody(responseBody.toJSONString());
	        awsProxyResObj.setStatusCode(401);
	    }
	    return awsProxyResObj;
	}

}

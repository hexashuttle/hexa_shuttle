package handler;//COD_BookingHandler


import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;
import org.json.simple.JSONObject;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import config.ShuttleConfig;
import controller.BookingProccessor;
import db.operartion.Helper;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;
import rds.ConnectionPool;
import request.BookingRequest;
import utils.AwsReqParserToPojo;


public class COD_BookingHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public COD_BookingHandler() throws Exception {
		System.out.println("\n COD_BookingHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }

    @SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {
    	

        String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
        System.out.println("Email : "+email);
        AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
        JSONObject responseBody = new JSONObject();
        if(email!=null && !email.equals("")){
        	context.getLogger().log("awsProxyReqObj: " + awsProxyReqObj);
            
            System.out.println("\n conObj = "+connObj);
            int statusCode=500;
            //parse aws req to my class
            AwsReqParserToPojo awsReqParserObj=new AwsReqParserToPojo();
            BookingRequest bookingObj= awsReqParserObj.getBookingRequest(awsProxyReqObj.getBody());
            Helper helpObj=new Helper();
            //Map cognito user email to db passengerId
            String trip_running_status=null;
            trip_running_status=helpObj.getTripStatus(bookingObj.getTrip_id(), dataSource, connObj);
            System.out.println("Trip running state : "+trip_running_status);
            boolean booking_allowed_in_the_trip=false;
            if(trip_running_status.equals(String.valueOf(ShuttleConfig.TRIP_INITIATED))
            ||trip_running_status.equals(String.valueOf(ShuttleConfig.ON_TRIP))){
            	booking_allowed_in_the_trip=true;
            	bookingObj.setPassenger_id(helpObj.getPassengerIdFromUserEmail(email, dataSource, connObj));
                bookingObj.setEmail(email);
                BookingProccessor bookProcObj=new BookingProccessor();
                Map<String,String> resMap=bookProcObj.crateBooking(bookingObj, dataSource, connObj); 
                
                if(resMap!=null && !resMap.isEmpty()){
                    responseBody.put("message",resMap.get("message") );
                    statusCode=Integer.parseInt(resMap.get("statusCode"));
                    if(resMap!=null && resMap.containsKey("booking_id")){
                    	responseBody.put("booking_id", resMap.get("booking_id"));
    	    		}
    	    		if(resMap!=null && resMap.containsKey("booking_number")){
    	    			responseBody.put("booking_number", resMap.get("booking_number"));
    	    		}
    	    		if(resMap!=null && resMap.containsKey("license_plate")){
    	    			responseBody.put("license_plate", resMap.get("license_plate"));
    	    		}
    	    		if(resMap!=null && resMap.containsKey("stop_name")){
    	    			responseBody.put("stop_name", resMap.get("stop_name"));
    	    		}
                }else{
                	responseBody.put("message","Booking failed." ); 
                }
            }else{
            	responseBody.put("message","Trip is completed." ); 
            	statusCode=400;
            }
            if(booking_allowed_in_the_trip){
            	awsProxyResObj.setBody(responseBody.toJSONString());
                awsProxyResObj.setStatusCode(statusCode);
            }else{
            	awsProxyResObj.setBody("Trip is completed.");
            	//awsProxyResObj.setBody(responseBody.toJSONString());
            	awsProxyResObj.setStatusCode(400);
            }
            
            
        }else{
        	responseBody.put("message", "Unauthorized");
        	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(401);
        }
        return awsProxyResObj;
    }
    
}


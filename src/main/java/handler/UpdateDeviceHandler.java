package handler;



import java.sql.Connection;
import javax.sql.DataSource;
import org.json.simple.JSONObject;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import rds.ConnectionPool;
import request.DeviceInfoRequest;
import utils.AwsReqParserToPojo;
import db.operartion.DeviceUpdate;
import db.operartion.Helper;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;

public class UpdateDeviceHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public UpdateDeviceHandler() throws Exception {
		System.out.println("\n UpdateDeviceHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }

    @SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {
    	String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
        System.out.println("Email : "+email);
        AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
        JSONObject responseBody = new JSONObject();
        int statusCode=500;
        if(email!=null && !email.equals("")){
        	
        	AwsReqParserToPojo awsReqParserObj=new AwsReqParserToPojo();
        	DeviceInfoRequest deviceInfoReqObj= awsReqParserObj.getDeviceInfoRequest(awsProxyReqObj.getBody());
            Helper helpObj=new Helper();
            //Map cognito user email to db passengerId
            deviceInfoReqObj.setUser_id(helpObj.getUserId(email, dataSource, connObj));
            System.out.println("deviceInfoReqObj user_id : "+deviceInfoReqObj.getUser_id());
            deviceInfoReqObj.setEmail(email);
        	System.out.println("deviceInfoReqObj: " + deviceInfoReqObj);
            System.out.println("\n connObj = "+connObj);
            DeviceUpdate deviceUpdateObj=new DeviceUpdate();
            boolean result=deviceUpdateObj.updateDevice(deviceInfoReqObj, dataSource, connObj);
            
            if(result){
            	 responseBody.put("message","success" );
                 statusCode=200;
            }else{
            	responseBody.put("message","failed" ); 
            }
            awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(statusCode);
        }else{
        	System.out.println("Email Not found.");
        	responseBody.put("message", "Unauthorized");
        	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(401);
        }
        return awsProxyResObj;
   
    
    }
}




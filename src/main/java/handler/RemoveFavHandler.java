package handler;//HandlerRemoveFav



import java.sql.Connection;
import java.util.Map;
import javax.sql.DataSource;
import org.json.simple.JSONObject;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import rds.ConnectionPool;
import request.MarkReqest;
import utils.AwsReqParserToPojo;
import db.operartion.Favourite;
import db.operartion.Helper;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;

public class RemoveFavHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {
	
	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public RemoveFavHandler() throws Exception {
		System.out.println("\n RemoveFavHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }

    @SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {

    	
    	String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
        System.out.println("Email : "+email);
        AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
        JSONObject responseBody = new JSONObject();
        int statusCode=500;
        if(email!=null && !email.equals("")){
        	
        	AwsReqParserToPojo awsReqParserObj=new AwsReqParserToPojo();
        	MarkReqest markObjParameter= awsReqParserObj.getMarkReqestRequest(awsProxyReqObj.getBody());
            Helper helpObj=new Helper();
            //Map cognito user email to db passengerId
            markObjParameter.setPassenger_id(helpObj.getPassengerIdFromUserEmail(email, dataSource, connObj));
            markObjParameter.setEmail(email);
        	System.out.println("markObjParameter: " + markObjParameter);
            System.out.println("\n connObj = "+connObj);
            Favourite favObj=new Favourite();
            Map<String,String> resMap=favObj.removeFavourite(markObjParameter,dataSource,connObj);
            if(resMap!=null && !resMap.isEmpty()){
            	 responseBody.put("message",resMap.get("message") );
                 statusCode=Integer.parseInt(resMap.get("statusCode"));
            }else{
            	responseBody.put("message","Removed favourite failed." ); 
            }
            awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(statusCode);
        }else{
        	System.out.println("Email Not found.");
        	responseBody.put("message", "Unauthorized");
        	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(401);
        }
        return awsProxyResObj;
   
    }
}


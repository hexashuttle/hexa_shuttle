package handler;

import java.sql.Connection;
import java.util.Map;

import javax.sql.DataSource;

import org.json.simple.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import config.ShuttleConfig;
import controller.BookingProccessor;
import controller.CancelBookingProcessor;
import db.operartion.Helper;
import dynamodb.NotifConfig;
import notif.InvokeSms;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;
import rds.ConnectionPool;
import request.BookingRequest;
import request.CancelBookingRequest;
import request.NotifDetails;
import utils.AwsReqParserToPojo;

public class CancelBookingHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {

	ConnectionPool jdbcObj;
	DataSource dataSource;
	Connection connObj;
	
	public CancelBookingHandler() throws Exception {
		System.out.println("\n CancelBookingHandler Constructor called..");
		this.jdbcObj = new ConnectionPool();
		this.dataSource = jdbcObj.setUpPool();
		jdbcObj.printDbStatus();
		connObj = dataSource.getConnection();
    }
	
    @SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {
    	

        String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
        System.out.println("Email : "+email);
        AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
        JSONObject responseBody = new JSONObject();
        if(email!=null && !email.equals("")){
        	context.getLogger().log("awsProxyReqObj: " + awsProxyReqObj);
            
            System.out.println("\n conObj = "+connObj);
            int statusCode=500;
            //parse aws req to my class
            AwsReqParserToPojo awsReqParserObj=new AwsReqParserToPojo();
            CancelBookingRequest cancelBookReqObj= awsReqParserObj.getCancelBookingRequest(awsProxyReqObj.getBody());
            Helper helpObj=new Helper();
            //Map cognito user email to db passengerId
            cancelBookReqObj.setPassenger_id(helpObj.getPassengerIdFromUserEmail(email, dataSource, connObj));
            Map<String,String> bookingDetailsMap=helpObj.getBookingNumberFromBookingId(cancelBookReqObj.getBooking_id(), dataSource, connObj);
            if(bookingDetailsMap!=null && !bookingDetailsMap.isEmpty()){
            	cancelBookReqObj.setBooking_number(bookingDetailsMap.get("booking_number"));
                cancelBookReqObj.setEmail(email);

            	String state=bookingDetailsMap.get("state");
            	if(state!=null && !state.equals(ShuttleConfig.BOOKING_CANCEL)){
            		CancelBookingProcessor canBookProcObj=new CancelBookingProcessor();
                    Map<String,String> resMap=canBookProcObj.canBookingProcessor(cancelBookReqObj, dataSource, connObj); 
                    
                    if(resMap!=null && !resMap.isEmpty()){
                        responseBody.put("message",resMap.get("message") );
                        statusCode=Integer.parseInt(resMap.get("statusCode"));
                    }else{
                    	responseBody.put("message","Cancel Booking failed." ); 
                    }
            	}else{
            		responseBody.put("message","Booking already cancelled." );
                    statusCode=400;
            	}
            	
            }
            
            awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(statusCode);
        }else{
        	responseBody.put("message", "Unauthorized");
        	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(401);
        }
        return awsProxyResObj;
    }
}


package handler;

import org.json.simple.JSONObject;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import config.ShuttleConfig;
import dynamodb.Configuratioin;
import proxy.model.AwsProxyRequest;
import proxy.model.AwsProxyResponse;

public class ConfigsHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {

    @SuppressWarnings("unchecked")
	@Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyReqObj, Context context) {
    	// TODO: implement your handler
    	String email=awsProxyReqObj.getRequestContext().getAuthorizer().getClaims().getEmail();
        System.out.println("Email : "+email);
        AwsProxyResponse awsProxyResObj=new AwsProxyResponse();
        JSONObject responseBody = new JSONObject();
        int statusCode=500;
        
        if(email!=null && !email.equals("")){
    		try {
    			Configuratioin configObj=new Configuratioin();
    			String configuaration=configObj.getConfig();
    			System.out.println("configuaration : "+configuaration);
    		    awsProxyResObj.setBody(configuaration);
    		    awsProxyResObj.setStatusCode(200);
    		            
    		 }
    		 catch (Exception e) {	           
    		    System.err.println("Exception at ConfigHandler : "+e.getMessage());
    		    responseBody.put("message", e.getMessage());
            	awsProxyResObj.setBody(responseBody.toJSONString());
                awsProxyResObj.setStatusCode(statusCode);
    		 }
        }else{
        	System.out.println("Email Not found.");
        	responseBody.put("message", "Unauthorized");
        	awsProxyResObj.setBody(responseBody.toJSONString());
            awsProxyResObj.setStatusCode(401);
        }
        return awsProxyResObj;
    }

}

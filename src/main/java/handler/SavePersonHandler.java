package handler;


import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import request.PersonRequest;
import response.PersonResponse;

public class SavePersonHandler implements RequestHandler<PersonRequest, PersonResponse> {
	
	private DynamoDB dynamoDB;
    //private String DYNAMODB_TABLE_NAME = "t_lock";
    private Regions REGION = Regions.AP_SOUTHEAST_1;
 
	
    public PersonResponse handleRequest(PersonRequest personRequest, Context context) {
    	  
    	this.initDynamoDbClient();
    	persistData(personRequest);
    	PersonResponse personResponse = new PersonResponse();
    	personResponse.setMessage("Saved Successfully!!!");
    	return personResponse;
     }
    private PutItemOutcome persistData(PersonRequest personRequest) 
    	      throws ConditionalCheckFailedException {
    	
    	PutItemOutcome outcome = null;
    	Table table = dynamoDB.getTable("t_lock");
    	
    	// Build the item
    	Item item = new Item()
    	    .withPrimaryKey("id",String.valueOf(System.currentTimeMillis()) )
    	    .withString("firstName", personRequest.getFirstName())
    	    .withString("lastName", personRequest.getLastName());
    	
    	// Write the item to the table 
    	try{
    		outcome = table.putItem(item);

    	}catch(Exception e){
    		System.err.println("Exception at persistData :"+e.getMessage());
    	}
    	return outcome;
    }
    private void initDynamoDbClient() {
    	AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
    	this.dynamoDB = new DynamoDB(client);
    }
    	 
}


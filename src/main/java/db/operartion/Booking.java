package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import request.BookingRequest;
import request.MarkReqest;
import utils.CustomeHelper;
import utils.FrameQuery;

public class Booking {
	@SuppressWarnings("unused")
	public Map<String,String> bookSeat(BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        Statement stmtObj2 = null;
        int update_result=0;
        int insert_result=0;
        int insert_passenger_trip=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
		Map<String,String> resMap=new HashMap<String,String>();

        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for bookSeat =====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);
        	String upTripSeatQuery=null;
        	String createBookingQuery=null;
        	String createPassengerTripQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	System.out.println("passenger_id :" +bookingObj.getPassenger_id());
        	Helper helpObj=new Helper();
        	
        	if(bookingObj.getPassenger_id()!=null && !bookingObj.getPassenger_id().equals("")){
        		String randomUUIDString=CustomeHelper.getBookingNumber();
        		resMap.put("booking_number", randomUUIDString);
        		createBookingQuery=frameObj.getBookingSeatQuery(bookingObj,randomUUIDString);
        	}
        	upTripSeatQuery=frameObj.updateTripSeatQuery(bookingObj);
        	if(upTripSeatQuery !=null && !upTripSeatQuery.equals("")){
        			//create fav
            		stmtObj1 = connObj.createStatement();
            		update_result = stmtObj1.executeUpdate(upTripSeatQuery);
                    System.out.println("Update count : "+update_result);
            }
        	if(update_result>0){
        		if(createBookingQuery !=null && !createBookingQuery.equals("")){
            		//create fav
            		stmtObj = connObj.createStatement();
                    insert_result = stmtObj.executeUpdate(createBookingQuery,Statement.RETURN_GENERATED_KEYS);
                    System.out.println("Insert count : "+insert_result);
                    System.out.println("\nBooking Done.");
                    rsObj=stmtObj.getGeneratedKeys();
                    while (rsObj.next()) {
                    	resMap.put("booking_id", String.valueOf(rsObj.getLong(1)));
                        System.out.println("booking_id : " + rsObj.getLong(1));
                    }
                    if(insert_result>0){
                    	//Create passenger trip
                    	System.out.println("Create Passenger Trip");
                    	PassengereTrip passengerTripObj=new PassengereTrip();
                    	createPassengerTripQuery=frameObj.getCreatePassengerTripQuery(bookingObj);//passengerTripObj.getPassengerTripQuer(bookingObj, dataSource, connObj);
                    	if(createPassengerTripQuery!=null && !createPassengerTripQuery.equals("")){
                    		stmtObj2 = connObj.createStatement();
                    		insert_passenger_trip = stmtObj2.executeUpdate(createPassengerTripQuery);
                            System.out.println("insert_passenger_trip : "+insert_passenger_trip);
                        }
                    }
                    String license_plate=helpObj.getVechileLicensePlate(bookingObj.getTrip_id(), dataSource, connObj);
                    if(license_plate!=null && !license_plate.equals("")){
                    	resMap.put("license_plate", license_plate);
                    }
                    String stop_name=helpObj.getStopName(bookingObj.getPick_stop_id(), dataSource, connObj);
                    if(stop_name!=null && !stop_name.equals("")){
                    	resMap.put("stop_name", stop_name);
                    }
                    resMap.put("result", "success");
                    connObj.commit();
                    
            	}
        	}else{
        		 resMap.put("result", "fail");
        	}
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: bookSeat "+sqlException.getMessage());
            operation=false;
            try {
				connObj.rollback();
				System.err.println("The transaction was rollback Exception : "+sqlException.getMessage());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println("There was an error making a rollback SQLException : "+e.getMessage());
				e.printStackTrace();
			}
        }
        return resMap;
    }
	@SuppressWarnings("unused")
	public Map<String,String> initiateBookSeat(BookingRequest bookingObj,
				DataSource dataSource,
				Connection connObj){
		ResultSet rsObj = null;
		Map<String,String> bookingMap=new HashMap<String,String>();
		
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result=0;
        int insert_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
        bookingMap.put("operation", "false");
        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for bookSeat =====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);
        	String upTripSeatQuery=null;
        	String createBookingQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	System.out.println("passenger_id :" +bookingObj.getPassenger_id());
        	bookingMap.put("passenger_id", bookingObj.getPassenger_id());
        	
        	if(bookingObj.getPassenger_id()!=null && !bookingObj.getPassenger_id().equals("")){
        		
        		createBookingQuery=frameObj.getBookingSeatQuery(bookingObj,"booking_number");
        	}
        	if(createBookingQuery !=null && !createBookingQuery.equals("")){
        		//create fav
        		stmtObj = connObj.createStatement();
                insert_result = stmtObj.executeUpdate(createBookingQuery,Statement.RETURN_GENERATED_KEYS);
                rsObj=stmtObj.getGeneratedKeys();
                //System.out.println("rsObj =="+rsObj.getFetchSize());
                while (rsObj.next()) {

                	bookingMap.put("booking_id", String.valueOf(rsObj.getLong(1)));
                    System.out.println("booking_id : " + rsObj.getLong(1));
                }
                System.out.println("Insert count : "+insert_result);
        	}
        	if(insert_result>0){
        		upTripSeatQuery=frameObj.updateTripSeatQuery(bookingObj);
        		if(upTripSeatQuery !=null && !upTripSeatQuery.equals("")){
        			//create fav
            		stmtObj1 = connObj.createStatement();
            		update_result = stmtObj1.executeUpdate(upTripSeatQuery);
                    System.out.println("Update count : "+insert_result);
                    System.out.println("\nBooking Done.");
                    operation=true;
                    bookingMap.put("operation", "true");

                    connObj.commit();
        		}
        	}
        } catch(Exception sqlException) {
            System.err.println("Exception  at: bookSeat "+sqlException.getMessage());
            operation=false;
            try {
				connObj.rollback();
				System.err.println("The transaction was rollback");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println("There was an error making a rollback");
				e.printStackTrace();
			}
        }
        System.out.println("bookingMap in initiateBookSeat : "+bookingMap);
        return bookingMap;
    }
		
}

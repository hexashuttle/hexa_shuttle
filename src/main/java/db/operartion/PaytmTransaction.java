package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.sql.DataSource;

import org.joda.time.LocalDateTime;
import org.json.simple.JSONObject;

import config.ShuttleConfig;
import rds.ConnectionPool;
import request.BookingRequest;
import request.MarkReqest;
import request.SuggestedRouteRequest;
import utils.FrameQuery;
import request.PaytmChecksumVarificationRequest;

public class PaytmTransaction {
	/**
	
	@SuppressWarnings({ "unused", "unchecked" })
	public boolean insertInPaymentRecord(BookingRequest ChecksumGenerationParameter,
			DataSource dataSource,
			Connection connObj,
			int booking_id){

        ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result=0;
        int insert_result1=0;
        int insert_result2=0;
        //int booking_id = 0;
        int passenger_id = 0;
        String insertQuery1="";
        String insertQuery2="";
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
        JSONObject json = new JSONObject();
        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
                connObj = dataSource.getConnection();
        	}
        	FrameQuery frameObj=new FrameQuery();
        	System.out.println("cust_id :" +ChecksumGenerationParameter.getCust_id());
        	System.out.println("order_id :" +ChecksumGenerationParameter.getOrder_id());
        	System.out.println("txn_amount :" +ChecksumGenerationParameter.getTxn_amount());
        	
        	//java.util.Date today = new java.util.Date();
        	Calendar calendar = Calendar.getInstance();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(calendar.getTime().getTime());
        	
        	
        	insertQuery1=frameObj.insertTransactionRecordQuery();
            
            if(insertQuery1 !=null && !insertQuery1.equals("")){
        		//create fav
        		//stmtObj = connObj.createStatement();
        		pstmtObj = connObj.prepareStatement(insertQuery1,Statement.RETURN_GENERATED_KEYS);
        		pstmtObj.setInt(1,Integer.parseInt(ChecksumGenerationParameter.getOrder_id()));
        		pstmtObj.setString(2,ChecksumGenerationParameter.getCust_id());
        		pstmtObj.setDouble(3,Double.parseDouble(ChecksumGenerationParameter.getTxn_amount()));
        		pstmtObj.setTimestamp(4,timestamp);
        		pstmtObj.setInt(5,passenger_id);
        		pstmtObj.setTimestamp(6,timestamp);
        		pstmtObj.setInt(7,passenger_id);
        		pstmtObj.setTimestamp(8,timestamp);
        		pstmtObj.executeUpdate();
                ResultSet keys = pstmtObj.getGeneratedKeys();
                //getSingleResult(insertQuery1);
                if (keys.next()) {
                	insert_result1 = keys.getInt(1);
                	System.out.println("txn_id== : "+insert_result1);
                }
                
                
                if(insert_result1>0) {
	                insertQuery2=frameObj.insertPaymentRecordQuery();
	                if(insertQuery2 !=null && !insertQuery2.equals("")) {
	                	
	                	pstmtObj = connObj.prepareStatement(insertQuery1,Statement.RETURN_GENERATED_KEYS);
	                	pstmtObj.setDouble(1,Double.parseDouble(ChecksumGenerationParameter.getTxn_amount()));
	            		pstmtObj.setInt(2,booking_id);//ChecksumGenerationParameter.getCust_id());
	            		pstmtObj.setInt(3,insert_result1);//Double.parseDouble(ChecksumGenerationParameter.getTxn_amount()));
	            		pstmtObj.setInt(4,Integer.parseInt(ShuttleConfig.PAYMENT_INITIATED));
	            		pstmtObj.setBoolean(5,false);//passenger_id);
	            		pstmtObj.setInt(6,passenger_id);
	            		pstmtObj.setTimestamp(7,timestamp);
	            		pstmtObj.setInt(8,passenger_id);
	            		pstmtObj.setTimestamp(9,timestamp);
	            		pstmtObj.executeUpdate();
	                    ResultSet keys2 = pstmtObj.getGeneratedKeys();
	                    //getSingleResult(insertQuery1);
	                    if (keys2.next()) {
	                    	insert_result2 = keys2.getInt(1);
	                    	System.out.println("payment_id== : "+insert_result2);
	                    }
	                }
                }
                
        	}
            operation=true;
            json.put("operation",operation);
            json.put("booking_id",booking_id);
            
            
        } catch(Exception sqlException) {
            System.err.println("Exception  at: PaytmTransaction "+sqlException.getMessage());
            operation=false;
            json.put("operation",operation);
        }
        return operation;
    }
	
	@SuppressWarnings({ "unused", "unchecked" })
	public boolean updatePaymentRecord(PaytmChecksumVarificationRequest ChecksumVarificationParameter,
			DataSource dataSource,
			Connection connObj) {
		
		ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result1=0;
        int update_result2=0;
        int booking_id = Integer.parseInt(ChecksumVarificationParameter.getBookingid());
        int booking_uuid = Integer.parseInt(ChecksumVarificationParameter.getOrderId());
        int passenger_id = 0;
        String updateQuery1="";
        String updateQuery2="";
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
        JSONObject json = new JSONObject();
        
        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
                connObj = dataSource.getConnection();
        	}
        	FrameQuery frameObj=new FrameQuery();
        	updateQuery1=frameObj.updateTransactionRecordQuery(ChecksumVarificationParameter);
        	updateQuery2=frameObj.updatePaymentRecordQuery(ChecksumVarificationParameter);
        	if(updateQuery1 !=null && !updateQuery1.equals("")) {
        		stmtObj = connObj.createStatement();
        		update_result1 = stmtObj.executeUpdate(updateQuery1);
                System.out.println("Update count : "+update_result1);
                operation=true;
        		
        	}
        	else {
        		operation=false;
        	}
        	if(updateQuery2 !=null && !updateQuery2.equals("")) {
        		stmtObj = connObj.createStatement();
        		update_result2 = stmtObj.executeUpdate(updateQuery2);
                System.out.println("Update count : "+update_result2);
                operation=true;
        		
        	}
        	else {
        		operation=false;
        	}
        
        
        }
        catch(Exception sqlException) {
        	System.err.println("Exception  at: markFavourite "+sqlException.getMessage());
            operation=false;
        	
        }
        
        return operation;
		
	}
	
//	public static void main(String args[]) {
//		System.out.println("=======");
//		
//		ConnectionPool jdbcObj;
//		DataSource dataSource=null;;
//		Connection connObj=null;;
//		try {
//		jdbcObj = new ConnectionPool();
//		dataSource = jdbcObj.setUpPool();
//		jdbcObj.printDbStatus();
//		connObj = dataSource.getConnection();
//		}
//		catch(Exception e) {
//			System.out.println(e.getMessage());
//		}
//		
//		PaytmChecksumRequest rqst = new PaytmChecksumRequest();
//		rqst.setCust_id("0123456789");
//		rqst.setCustomer_email("test@test.com");
//		rqst.setCustomer_mobile("0123456789");
//		rqst.setTxn_amount("99.00");
//		rqst.setOrder_id("456789");
//		
//		PaytmTransaction pay = new PaytmTransaction();
//		pay.insertInPaymentRecord(rqst, dataSource, connObj);
//		
//	}
 * 
 */

}

package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import request.PickUpByPassengerRequest;
import utils.FrameQuery;

public class PickUpByPassenger {
	
	@SuppressWarnings("unused")
	public Map<String,String> pickUp(PickUpByPassengerRequest pickUpPassReqObj,
			DataSource dataSource,
			Connection connObj){
		
		ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj1 = null;
        Statement stmtObj2 = null;
        int update_passenger_booking_result=0;
        int update_passenger_trip_for_booking_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
		Map<String,String> resMap=new HashMap<String,String>();
		try{
			// Performing Database Operation!
			System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for PickUpByPassenger pickUp =====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);
        	String updateBookingQuery=null;
        	String updatePassengerTripQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	updateBookingQuery=frameObj.getUpdateBookingQuery(pickUpPassReqObj);
        	if(updateBookingQuery!=null && !updateBookingQuery.equals("")){
        		stmtObj1 = connObj.createStatement();
        		update_passenger_booking_result = stmtObj1.executeUpdate(updateBookingQuery);
                System.out.println("update_passenger_booking_result : "+update_passenger_booking_result);
                
                if(update_passenger_booking_result>0){
                	updatePassengerTripQuery=frameObj.getPassengerTripPickUpQuery(pickUpPassReqObj);
                	if(updatePassengerTripQuery!=null && !updatePassengerTripQuery.equals("")){
                		stmtObj2 = connObj.createStatement();
                		update_passenger_trip_for_booking_result = stmtObj2.executeUpdate(updatePassengerTripQuery);
                        System.out.println("update_passenger_trip_for_booking_result : "+update_passenger_trip_for_booking_result);
                        if(update_passenger_trip_for_booking_result>0){
                        	System.out.println("\nPickUp done by passenger.");
                            operation=true;
                            connObj.commit();
                            resMap.put("message", "PickUp by passenger sucessfully.");
            	    		resMap.put("statusCode", "200");
                        	
                        }else{
                        	resMap.put("message", "PickUp by passenger failed.");
            	    		resMap.put("statusCode", "500");
                        }
                    }else{
                    	resMap.put("message", "PickUp by passenger failed.");
        	    		resMap.put("statusCode", "500");
                    }
                }else{
                	resMap.put("message", "PickUp by passenger failed.");
    	    		resMap.put("statusCode", "500");
                }
        	}else{
            	resMap.put("message", "PickUp by passenger failed.");
	    		resMap.put("statusCode", "500");
            }
        	
		} catch(Exception sqlException) {
            System.err.println("Exception  at: PickUpByPassenger pickUp  "+sqlException.getMessage());
            resMap.put("message", sqlException.getMessage());
    		resMap.put("statusCode", "500");
            try {
				connObj.rollback();
				System.err.println("The transaction was rollback Exception : "+sqlException.getMessage());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println("There was an error making a rollback SQLException : "+e.getMessage());
				e.printStackTrace();
			}
        }
		System.out.println("PickUp done by passenger resMap : "+resMap);
        return resMap;
	}

}

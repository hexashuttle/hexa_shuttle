package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import config.ShuttleConfig;
import request.BookingRequest;
import request.MarkReqest;
import utils.FrameQuery;
import utils.UserConfigMapper;

public class Helper {
	
	@SuppressWarnings("unused")
	public Map<String,String> getAvailableSeat(BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Map<String,String> seatMap=new HashMap<String,String>();
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAvailableSeat=====\n");
                connObj = dataSource.getConnection();
        	}
        	//String getAvailableSeatQuery="select total_seats,remaining_seats from trip where id="+bookingObj.getTrip_id();
        	String getAvailableSeatQuery="select v.license_plate,t.total_seats,t.remaining_seats,t.id from trip t "
        			+ " INNER JOIN vehicle as v ON (v.id = t.vehicle_id) where t.id="+bookingObj.getTrip_id();
        	System.out.println("getAvailableSeatQuery Query = "+getAvailableSeatQuery);
            pstmtObj = connObj.prepareStatement(getAvailableSeatQuery);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	seatMap.put("license_plate", rsObj.getString("license_plate"));
            	seatMap.put("trip_id", String.valueOf(rsObj.getInt("id")));
            	seatMap.put("total_seats", String.valueOf(rsObj.getInt("total_seats")));
            	seatMap.put("remaining_seats", String.valueOf(rsObj.getInt("remaining_seats")));
            	System.out.println("Total seats : " + seatMap.get("total_seats")+" available for booking : "+seatMap.get("remaining_seats"));
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getAvailableSeat "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return seatMap;
    }
	
	@SuppressWarnings("unused")
	public String getPassengerFromUser(String user_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String passenger_id=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getPassengerFromUser=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getPassengerFromUserQ="select id from passenger where user_id="+user_id;
        	System.out.println("getPassengerFromUser Query = "+getPassengerFromUserQ);
            pstmtObj = connObj.prepareStatement(getPassengerFromUserQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	passenger_id=rsObj.getString("id");
            	System.out.println("Passenger Id :"+passenger_id);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getPassengerFromUser "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return passenger_id;
    }
	
	@SuppressWarnings("unused")
	public String getUserMobile(String email,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String mobile=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getUserMobile=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getPassengerFromUserQ="select mobile from usr where email='"+email+"'";
        	System.out.println("getPassengerFromUser Query = "+getPassengerFromUserQ);
            pstmtObj = connObj.prepareStatement(getPassengerFromUserQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	mobile=rsObj.getString("mobile");
            	System.out.println("mobile :"+mobile);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getUserMobile "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return mobile;
    }
	
	@SuppressWarnings("unused")
	public String getUserId(String email,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String user_id=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getUserIdQ=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getUserIdQ="select id from usr where email='"+email+"'";
        	System.out.println("getUserId Query = "+getUserIdQ);
            pstmtObj = connObj.prepareStatement(getUserIdQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	user_id=rsObj.getString("id");
            	System.out.println("user_id :"+user_id);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getUserIdQ "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return user_id;
    }
	
	@SuppressWarnings("unused")
	public UserConfigMapper getUserSettings(String email,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String user_id=null;
        UserConfigMapper userConfigMapObj = null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getUserSettings=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getUserIdQ="select arrival_notif,is_sms,is_push,is_mail,is_voice from usr_config "
        			+ "  where usr_id=(select id from usr where email='"+email+"')  limit 1 ";
        	System.out.println("getUserId Query = "+getUserIdQ);
            pstmtObj = connObj.prepareStatement(getUserIdQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	userConfigMapObj=new UserConfigMapper();
            	userConfigMapObj.setArrival_notif(rsObj.getInt("arrival_notif"));
            	userConfigMapObj.setIs_sms(rsObj.getBoolean("is_sms"));
            	userConfigMapObj.setIs_push(rsObj.getBoolean("is_push"));
            	userConfigMapObj.setIs_mail(rsObj.getBoolean("is_mail"));
            	userConfigMapObj.setIs_voice(rsObj.getBoolean("is_voice"));
            	System.out.println("userConfigMapObj SMS :"+userConfigMapObj.is_sms);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getUserSettings "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("userConfigMapObj : "+userConfigMapObj);
        return userConfigMapObj;
    }
	
	@SuppressWarnings("unused")
	public UserConfigMapper getUserSettingsFromPassengerId(String passenger_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String user_id=null;
        UserConfigMapper userConfigMapObj = null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getUserSettings=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getUserIdQ="select arrival_notif,is_sms,is_push,is_mail,is_voice from usr_config "
        			+ "  where usr_id=(select user_id from passenger where id='"+passenger_id+"')  limit 1 ";
        	System.out.println("getUserId Query = "+getUserIdQ);
            pstmtObj = connObj.prepareStatement(getUserIdQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	userConfigMapObj=new UserConfigMapper();
            	userConfigMapObj.setArrival_notif(rsObj.getInt("arrival_notif"));
            	userConfigMapObj.setIs_sms(rsObj.getBoolean("is_sms"));
            	userConfigMapObj.setIs_push(rsObj.getBoolean("is_push"));
            	userConfigMapObj.setIs_mail(rsObj.getBoolean("is_mail"));
            	userConfigMapObj.setIs_voice(rsObj.getBoolean("is_voice"));
            	System.out.println("userConfigMapObj SMS :"+userConfigMapObj.is_sms);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getUserSettings "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("userConfigMapObj : "+userConfigMapObj);
        return userConfigMapObj;
    }
	
	@SuppressWarnings("unused")
	public String getUserPushToken(String email,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String push_token=null;
        try {
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getUserPushToken=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getUserPushToken="select push_token from device where "
        			+ " user_id =(select id from usr where email='"+email+"') and "
        			+ " status='active' ";
        	System.out.println("getUserPushToken Query = "+getUserPushToken);
            pstmtObj = connObj.prepareStatement(getUserPushToken);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	push_token=rsObj.getString("push_token");
            	System.out.println("push_token :"+push_token);
            }
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getUserPushToken "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return push_token;
    }
	
	@SuppressWarnings("unused")
	public String getPassengerIdFromUserEmail(String email,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String passenger_id=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getPassengerIdFromUserEmail=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getPassengerIdFromUserEmail="select id from passenger where"
        			+ "  user_id=(select id from usr where email='"+email+"')";
        	System.out.println("getPassengerIdFromUserEmail Query = "+getPassengerIdFromUserEmail);
            pstmtObj = connObj.prepareStatement(getPassengerIdFromUserEmail);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	passenger_id=rsObj.getString("id");
            	System.out.println("Passenger Id :"+passenger_id);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getPassengerIdFromUserEmail "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return passenger_id;
    }
	
	@SuppressWarnings("unused")
	public String getVechileLicensePlate(String trip_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String license_plate="";
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAvailableSeat=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getBookingDetailQ="select v.license_plate from trip t "
        			+ " INNER JOIN vehicle as v ON (v.id = t.vehicle_id) where t.id="+trip_id;
        	System.out.println("getAvailableSeatQuery Query = "+getBookingDetailQ);
            pstmtObj = connObj.prepareStatement(getBookingDetailQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	license_plate=rsObj.getString("license_plate");//seatMap.put("license_plate", rsObj.getString("license_plate"));
            	System.out.println("license_plate : " + license_plate);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getBookingDetail "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return license_plate;
    }
	@SuppressWarnings("unused")
	public String getStopName(String pick_stop_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String stop_name="";
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAvailableSeat=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getStopNameQ="select name from stop where id="+pick_stop_id;
        	System.out.println("getStopNameQ Query = "+getStopNameQ);
            pstmtObj = connObj.prepareStatement(getStopNameQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	stop_name=rsObj.getString("name");
            	System.out.println("stop_name : " + stop_name);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getStopName "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return stop_name;
    }
	
	@SuppressWarnings("unused")
	public boolean getUSerDevice(String user_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        boolean device_present=false;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAvailableSeat=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getUSerDevice="select id from device where user_id="+user_id;
        	System.out.println("getUSerDevice Query = "+getUSerDevice);
            pstmtObj = connObj.prepareStatement(getUSerDevice);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	device_present=true;
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getStopName "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return device_present;
    }
	@SuppressWarnings("unused")
	public String getTripStatus(String trip_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String trip_running_status=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getTripStatus=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getTripStatus="select state from trip where id="+trip_id;
        	System.out.println("getTripStatus Query = "+getTripStatus);
            pstmtObj = connObj.prepareStatement(getTripStatus);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	trip_running_status=rsObj.getString("state");
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getTripStatus "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return trip_running_status;
    }
	

	@SuppressWarnings("unused")
	public Map<String,String> getBookingNumberFromBookingId(String booking_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String trip_running_status=null;
		Map<String,String> resMap=new HashMap<String,String>();

        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getBookingNumberFromBookingId=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getBookingNumberFromBookingId="select booking_number,state from booking trip where id="+booking_id;
        	System.out.println("getBookingNumberFromBookingId Query = "+getBookingNumberFromBookingId);
            pstmtObj = connObj.prepareStatement(getBookingNumberFromBookingId);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	trip_running_status=rsObj.getString("state");
            	resMap.put("booking_number", rsObj.getString("booking_number"));
            	resMap.put("state", rsObj.getString("state"));
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getBookingNumberFromBookingId "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("getBookingNumberFromBookingId : "+resMap);
        return resMap;
    }
	
	@SuppressWarnings("unused")
	public String getTripFromBookingId(String booking_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String trip_id=null;
        try {

            // Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getTripStatus=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getTripFromBookingId="select trip_id from booking where id="+booking_id+" "
        			+ " and state='"+ShuttleConfig.BOOKING_CONFIRMED+"'";
        	System.out.println("getTripFromBookingId Query = "+getTripFromBookingId);
            pstmtObj = connObj.prepareStatement(getTripFromBookingId);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	trip_id=rsObj.getString("trip_id");
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getTripFromBookingId "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("trip_id : "+trip_id);
        return trip_id;
    }
	
	@SuppressWarnings("unused")
	public String getDriverPushTokenFromTrip(String trip_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String push_token=null;
        try {
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getDriverPushTokenFromTrip=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getDriverPushTokenFromTrip="select push_token from device"
        			+ " where user_id =(select user_id from driver "
        			+ " where id=(select driver_id from trip where id="+trip_id+" )) and "
        			+ " status='active' ";
        	System.out.println("getDriverPushTokenFromTrip Query = "+getDriverPushTokenFromTrip);
            pstmtObj = connObj.prepareStatement(getDriverPushTokenFromTrip);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	push_token=rsObj.getString("push_token");
            	System.out.println("push_token :"+push_token);
            }
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getDriverPushTokenFromTrip "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return push_token;
    }
	@SuppressWarnings("unused")
	public String getPassengerNameFromPassengerID(String passenger_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String name=null;
        try {
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getPassengerNameFromPassengerID=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getPassengerNameFromPassengerID="select name from passenger where id="+passenger_id;
        	System.out.println("getPassengerNameFromPassengerID Query = "+getPassengerNameFromPassengerID);
            pstmtObj = connObj.prepareStatement(getPassengerNameFromPassengerID);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	name=rsObj.getString("name");
            	System.out.println("name :"+name);
            }
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getPassengerNameFromPassengerID "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("name : "+name);
        return name;
    }
	
	
	
	
	
}

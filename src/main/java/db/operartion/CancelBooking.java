package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import request.CancelBookingRequest;
import utils.CustomeHelper;
import utils.FrameQuery;

public class CancelBooking {
	
	@SuppressWarnings("unused")
	public Map<String,String> canBookSeat(CancelBookingRequest cancelBookReqObj,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        Statement stmtObj2 = null;
        int update_booking_result=0;
        int update_trip_result=0;
        int update_passenger_trip_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
		Map<String,String> resMap=new HashMap<String,String>();

        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for bookSeat =====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);
        	String decrementTripSeatQuery=null;
        	String cancelBookingQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	
        	if(cancelBookReqObj.getBooking_id()!=null && !cancelBookReqObj.getBooking_id().equals("")){
        		
        		cancelBookingQuery=frameObj.getCancelBookingQuery(cancelBookReqObj);
        	}
        	if(cancelBookReqObj.getTrip_id()!=null && !cancelBookReqObj.getTrip_id().equals("")){
        		decrementTripSeatQuery=frameObj.getIncrementTripSeatQuery(cancelBookReqObj);
        	}
        	if(cancelBookingQuery !=null && !cancelBookingQuery.equals("")){
        		stmtObj1 = connObj.createStatement();
            	update_booking_result = stmtObj1.executeUpdate(cancelBookingQuery);
                System.out.println("update_booking_result : "+update_booking_result);
            }
        	if(update_booking_result>0){
        		if(decrementTripSeatQuery !=null && !decrementTripSeatQuery.equals("")){
            		//create fav
            		stmtObj = connObj.createStatement();
            		update_trip_result = stmtObj.executeUpdate(decrementTripSeatQuery);
                    System.out.println("update_trip_result : "+update_trip_result);
                    if(update_trip_result>0){
                    	String updatePassengerTrip=frameObj.getPassengerTripCancelQuery(cancelBookReqObj);
                    	if(updatePassengerTrip!=null && !updatePassengerTrip.equals("")){
                    		stmtObj2 = connObj.createStatement();
                    		update_passenger_trip_result = stmtObj2.executeUpdate(updatePassengerTrip);
                            System.out.println("update_passenger_trip_result : "+update_passenger_trip_result);

                    	}
                    }
                    System.out.println("\nBooking Cancel Done.");
                    operation=true;
                    connObj.commit();
                    resMap.put("message", "Booking Cancelled.");
    	    		resMap.put("statusCode", "200");
            	}
        	}else{
        		resMap.put("message", "Booking Cancelled failed.");
	    		resMap.put("statusCode", "500");
        	}
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: canBookSeat "+sqlException.getMessage());
            resMap.put("message", sqlException.getMessage());
    		resMap.put("statusCode", "500");
            try {
				connObj.rollback();
				System.err.println("The transaction was rollback Exception : "+sqlException.getMessage());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println("There was an error making a rollback SQLException : "+e.getMessage());
				e.printStackTrace();
			}
        }
        System.out.println("Cancel Booking resMap : "+resMap);
        return resMap;
    
	}

}

package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import config.ShuttleConfig;
import utils.FrameQuery;

public class SaveNoitification {
	
	@SuppressWarnings("unused")
	public boolean insertSms(String email,
			String msgBody,
			String state,
			String notif_type,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        boolean save=false;
        int insert_count=0;
        try {

            // Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for insertSms=====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);

        	FrameQuery frameQ=new FrameQuery();
        	String getInsertSms=frameQ.getInsertNotifQuery(email,
        			msgBody,
        			ShuttleConfig.BOOKING_CONFIRMED,
        			ShuttleConfig.IS_SMS);
        	System.out.println("getInsertSms Query = "+getInsertSms);
        	stmtObj = connObj.createStatement();
        	insert_count = stmtObj.executeUpdate(getInsertSms);
        	System.out.println("Sms insert_count : "+insert_count);
        	if(insert_count>0){
        		save=true;
        	}
            connObj.commit();

        } catch(Exception sqlException) {
            System.out.println("Exception  at: insertSms "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("Save : "+save);
        return save;
    }
	
	@SuppressWarnings("unused")
	public boolean insertPush(String email,
			String msgBody,
			String state,
			String notif_type,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        boolean save=false;
        int insert_count=0;
        try {

            // Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for insertPush=====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);

        	FrameQuery frameQ=new FrameQuery();
        	String getInsertPush=frameQ.getInsertNotifQuery(email,
        			msgBody,
        			ShuttleConfig.BOOKING_CONFIRMED,
        			ShuttleConfig.IS_PUSH);
        	System.out.println("insertPush Query = "+getInsertPush);
        	stmtObj = connObj.createStatement();
        	insert_count = stmtObj.executeUpdate(getInsertPush);
        	System.out.println("Sms insert_count : "+insert_count);
        	if(insert_count>0){
        		save=true;
        	}
            connObj.commit();

        } catch(Exception sqlException) {
            System.out.println("Exception  at: insertPush "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("Save : "+save);
        return save;
    
		
	}

}

package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import config.ShuttleConfig;
import request.BookingRequest;
import scala.Array;

public class PassengereTrip {
	
	
	public String getPassengerTripQuer(BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj){
		String insertPassengerTripQuery=null;
		String route_timings_id=getRouteTimingsId(bookingObj.getTrip_id(),dataSource,connObj);
		if(route_timings_id!=null && !route_timings_id.equals("")){
			Map<String,String> resMapOfRouteDetails=getRouteDetails(route_timings_id,dataSource,connObj);
			if(resMapOfRouteDetails!=null && resMapOfRouteDetails.size()>0){
				int startTimeMinute=Integer.parseInt(resMapOfRouteDetails.get("minute"));
				String route_id=resMapOfRouteDetails.get("route_id");
				
				Map<String,String> resMapStopSeq=getStopsSeq(route_id, bookingObj.getPick_stop_id(), bookingObj.getDrop_stop_id(), dataSource, connObj);
				if(resMapStopSeq !=null && resMapStopSeq.size()>0){
					int pickUpStopSeq=Integer.parseInt(resMapStopSeq.get(bookingObj.getPick_stop_id()));
					int dropStopSeq=Integer.parseInt(resMapStopSeq.get(bookingObj.getDrop_stop_id()));
					boolean isSequenseWise=false;
					if(pickUpStopSeq!=0  && dropStopSeq!=0){
						if(pickUpStopSeq<dropStopSeq){
							isSequenseWise=true;
						}
					}
					List<String> resList=getAllStop(route_id, 
							bookingObj.getPick_stop_id(), 
							bookingObj.getDrop_stop_id(), 
							isSequenseWise, 
							dataSource, 
							connObj);
					if(resList!=null && resList.size()>0){
						Map<String,String> getActualStopAvgTime=getAllStopAverageWaitingTime(route_id,dataSource,connObj);
						System.out.println("getActualStopAvgTime : "+getActualStopAvgTime);
						Map<String,String> getModifiedStopAvgTime=new HashMap<String, String>();
						if(getActualStopAvgTime!=null && getActualStopAvgTime.size()>0){
							String stop_interval="";
							String stop_ids="";
							int count=1;
							for (Map.Entry<String,String> entry : getActualStopAvgTime.entrySet()) {
								System.out.println("Key = " + entry.getKey() +", Value = " + entry.getValue());
								if(count==1){
					            	getModifiedStopAvgTime.put(entry.getKey(), String.valueOf(startTimeMinute));
					            }else{
					            	startTimeMinute=startTimeMinute+Integer.parseInt(entry.getValue());
					            	getModifiedStopAvgTime.put(entry.getKey(), String.valueOf(startTimeMinute));
					            }
								count=count+1;
					        }
							System.out.println("getModifiedStopAvgTime : "+getModifiedStopAvgTime);
							if(getModifiedStopAvgTime!=null && getModifiedStopAvgTime.size()>0){
								for (int x = 0; x < resList.size(); x++) {
									if (x != (resList.size() - 1)) {
										stop_interval =stop_interval+""+ getModifiedStopAvgTime.get(resList.get(x))+" ,";
										stop_ids =stop_ids+""+ resList.get(x)+" ,";

									} else {
										stop_interval =stop_interval+""+getModifiedStopAvgTime.get(resList.get(x));
										stop_ids =stop_ids+""+resList.get(x);
									}
							}
							if( (stop_ids!=null && !stop_ids.equals("")) && 
									(stop_interval!=null && !stop_interval.equals(""))){
								String actual_stop_ids="{"+stop_ids+"}";
								String actual_stop_interval="{"+stop_interval+"}";
								System.out.println("actual_stop_ids : "+actual_stop_ids);
								System.out.println("actual_stop_interval : "+actual_stop_interval);
								
								//Execute that process
								insertPassengerTripQuery="insert into passenger_trip "
										+ " (passenger_id,pick_stop_id,drop_stop_id,state,trip_id,"
										+ " fare,stops,stops_interval,created_by,created_at,last_modified_by,last_modified_at) "
					    					+ "values "
					    					+ "("+bookingObj.getPassenger_id()+","+bookingObj.getPick_stop_id()+","
					    					+ ""+bookingObj.getDrop_stop_id()+","+ShuttleConfig.PASSENGER_TRIP_CREATED+","+bookingObj.getTrip_id()+","
					    					+ "(select fare from trip where id="+bookingObj.getTrip_id()+"),"
					    					+ " '"+actual_stop_ids+"','"+actual_stop_interval+"',"
					    					+ " "+bookingObj.getPassenger_id()+",now(),"+bookingObj.getPassenger_id()+",now())";
					    		System.out.println("insertPassengerTripQuery : "+insertPassengerTripQuery);
								
							}
							
						}
					}
					
				}
				
			}
		}
	}
	return insertPassengerTripQuery;
	}
	
	//select route_timings_id from trip where id=1
	@SuppressWarnings("unused")
	public String getRouteTimingsId(String trip_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        String route_timings_id=null;
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getRouteTimingsId=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getRouteTimingsIdQ="select route_timings_id from trip where id="+trip_id;
        	System.out.println("getRouteTimingsIdQ Query = "+getRouteTimingsIdQ);
            pstmtObj = connObj.prepareStatement(getRouteTimingsIdQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	route_timings_id=rsObj.getString("route_timings_id");
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getRouteTimingsId "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return route_timings_id;
    }
	//select start_time from route_timings where route_id=1
	@SuppressWarnings("unused")
	public Map<String,String> getRouteDetails(String route_timings_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Map<String,String> resMap=new HashMap<String, String>();
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getRouteTimingsId=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getRouteDetailsQ="select extract(minute from start_time) as start_time_minute,route_id from route_timings where id="+route_timings_id;
        	System.out.println("getRouteDetailsQ Query = "+getRouteDetailsQ);
            pstmtObj = connObj.prepareStatement(getRouteDetailsQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	resMap.put("minute", rsObj.getString("start_time_minute"));
            	resMap.put("route_id", rsObj.getString("route_id"));
            }
            
        } catch(Exception sqlException) {
            System.err.println("Exception  at: getRouteTimingsId "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("getRouteDetails resMap : "+resMap);
        return resMap;
    }
	//select stop_id,stop_sequence from stop_duration where stop_id in (2,4) and route_id=1
	@SuppressWarnings("unused")
	public Map<String,String> getStopsSeq(String route_id,
			String pick_stop_id,
			String drop_stoop_id,
			DataSource dataSource,
			Connection connObj){

		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Map<String,String> resMap=new HashMap<String, String>();
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getStopsSeqQ=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getStopsSeqQ="select stop_id,stop_sequence from stop_duration "
        			+ " where stop_id in ("+pick_stop_id+","+drop_stoop_id+") "
        			+ " and route_id="+route_id+" order by stop_sequence";
        	System.out.println("getStopsSeqQ Query = "+getStopsSeqQ);
            pstmtObj = connObj.prepareStatement(getStopsSeqQ);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	resMap.put(rsObj.getString("stop_id"), rsObj.getString("stop_sequence"));
            }
            
        } catch(Exception sqlException) {
            System.err.println("Exception  at: getStopsSeqQ "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return resMap;
    }
	
	@SuppressWarnings("unused")
	public List<String>  getAllStop(String route_id,
			String pick_stop_id,
			String drop_stoop_id,
			boolean isSequenseWise,
			DataSource dataSource,
			Connection connObj){


		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Map<String,String> resMap=new HashMap<String, String>();
        String getAllStop=null;
        List<String> listOfAllStopIds=new ArrayList<String>();
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAllStop=====\n");
                connObj = dataSource.getConnection();
        	}
        	if(isSequenseWise){
        		getAllStop="select stop_id from stop_duration where route_id="+route_id+" "
        				+ " and stop_sequence >= (select stop_sequence from stop_duration "
        				+ " where route_id="+route_id+" and stop_id="+pick_stop_id+") "
        				+ " and stop_sequence <= (select stop_sequence from stop_duration where route_id="+route_id+" "
        				+ " and stop_id="+drop_stoop_id+")"
        				+ " order by stop_sequence";
        	}else{
        		getAllStop="select stop_id from stop_duration where route_id="+route_id+" "
        				+ " and stop_sequence >= (select stop_sequence from stop_duration "
        				+ " where route_id="+route_id+" and stop_id="+drop_stoop_id+") "
        				+ " and stop_sequence <= (select stop_sequence from stop_duration where route_id="+route_id+" "
        				+ " and stop_id="+pick_stop_id+")"
        				+ " order by stop_sequence";
        	}
        	
        	System.out.println("getAllStop Query = "+getAllStop);
            pstmtObj = connObj.prepareStatement(getAllStop);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	listOfAllStopIds.add(rsObj.getString("stop_id"));
            }
            
        } catch(Exception sqlException) {
            System.err.println("Exception  at: getAllStop "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("listOfAllStopIds : "+listOfAllStopIds);
        return listOfAllStopIds;
    }
	
	@SuppressWarnings("unused")
	public Map<String,String>  getAllStopAverageWaitingTime(String route_id,
			DataSource dataSource,
			Connection connObj){
		ResultSet rsObj = null;
        PreparedStatement pstmtObj = null;
        Map<String,String> resMap=new HashMap<String, String>();
        List<String> listOfAllStopIds=new ArrayList<String>();
        try {

        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getAllStopAverageWaitingTime=====\n");
                connObj = dataSource.getConnection();
        	}
        	String getAllStop="select stop_id,avg_duration from stop_duration  "
        			+ " where route_id="+route_id+" order by stop_sequence ";
        	
        	
        	System.out.println("getAllStopAverageWaitingTime Query = "+getAllStop);
            pstmtObj = connObj.prepareStatement(getAllStop);
            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {
            	resMap.put(rsObj.getString("stop_id"), rsObj.getString("avg_duration"));
            }
            
        } catch(Exception sqlException) {
            System.err.println("Exception  at: getAllStopAverageWaitingTime "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        System.out.println("resMap : "+resMap);
        return resMap;
    
		
	}
	
	



}

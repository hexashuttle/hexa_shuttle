package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import request.BookingRequest;
import request.DeviceInfoRequest;
import utils.CustomeHelper;
import utils.FrameQuery;

public class DeviceUpdate {
	
	@SuppressWarnings("unused")
	public boolean updateDevice(DeviceInfoRequest deviceUpdateObj,
			DataSource dataSource,
			Connection connObj){
		

		ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int device_update_result=0;
        int device_insert_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
		Map<String,String> resMap=new HashMap<String,String>();

        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for bookSeat =====\n");
                connObj = dataSource.getConnection();
        	}
        	connObj.setAutoCommit(false);
        	String upTripSeatQuery=null;
        	String createBookingQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	Helper helpObj=new Helper();
        	//device exit or not
        	boolean device_exits=helpObj.getUSerDevice(deviceUpdateObj.getUser_id(), dataSource, connObj);
        	System.out.println("device_exits : "+device_exits);
        	
        	if(device_exits){
        		//update device
        		String updateDeviceQuery=frameObj.getDeviceUpdateQuery(deviceUpdateObj);
        		if(updateDeviceQuery!=null && !updateDeviceQuery.equals("")){
        			stmtObj = connObj.createStatement();
        			device_update_result = stmtObj.executeUpdate(updateDeviceQuery);
                    System.out.println("device_update_result : "+device_update_result);
        		}
        		
        	}else{
        		//insert device
        		String deviceInsertQuery=frameObj.getDeviceInsertQuery(deviceUpdateObj);
        		if(deviceInsertQuery!=null && !deviceInsertQuery.equals("")){
        			stmtObj1 = connObj.createStatement();
        			device_insert_result = stmtObj1.executeUpdate(deviceInsertQuery);
                    System.out.println("device_insert_result : "+device_insert_result);
        		}
        	}
        	if(device_update_result>0 || device_insert_result>0){
        		operation=true;
        	}
        	 connObj.commit();
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: updateDevice "+sqlException.getMessage());
            operation=false;
            try {
				connObj.rollback();
				System.err.println("updateDevice The transaction was rollback Exception : "+sqlException.getMessage());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println(" updateDevice There was an error making a rollback SQLException : "+e.getMessage());
				e.printStackTrace();
			}
            
        }
        return operation;
    }

}

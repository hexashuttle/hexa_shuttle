package db.operartion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import config.ShuttleConfig;
import rds.ConnectionPool;
import request.MarkReqest;
import request.SuggestedRouteRequest;
import utils.FrameQuery;

public class Favourite {
	
    @SuppressWarnings("unused")
	public Map<String,String> markFavourite(MarkReqest markObj,
			DataSource dataSource,
			Connection connObj){

        ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result=0;
        int insert_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        Map<String,String> resMap=new HashMap<String, String>();
        Map<String,String> innerResMap=new HashMap<String, String>();
        boolean operation=false;
        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
                connObj = dataSource.getConnection();
        	}
        	Helper helperObj=new Helper();
        	FrameQuery frameObj=new FrameQuery();
        	String upFaveQuery=null;
        	String createFaveQuery=null;


        	System.out.println("markObj.getPassenger_id() :"+markObj.getPassenger_id());
        	
        	innerResMap=getPassengerFav(markObj,dataSource,connObj);
        	System.out.println("innerResMap : "+innerResMap);
        	if(innerResMap!=null && !innerResMap.isEmpty()){
        		if(innerResMap.get("state").equals(ShuttleConfig.FAVOURITE)){
        			System.out.println("Already favourite.");
        			//resMap.put("message", "already favourite");
        		}else if(innerResMap.get("state").equals(ShuttleConfig.REMOVED_FROM_FAVOURITE)){
        			//update that route to become favorite
        			System.out.println("Make favorite again");
        			upFaveQuery=frameObj.getRemoveFavToFavQuery(markObj);
        		}
        	}else{
    			System.out.println("create favorite route ");
    			createFaveQuery=frameObj.getMarkFavQuery(markObj);
        	}
        	
        	if(upFaveQuery !=null && !upFaveQuery.equals("")){
        		//Update fav 
        		stmtObj = connObj.createStatement();
                update_result = stmtObj.executeUpdate(upFaveQuery);
                System.out.println("Update count : "+update_result);
        	}
        	if(createFaveQuery !=null && !createFaveQuery.equals("")){
        		//create fav
        		stmtObj1 = connObj.createStatement();
                insert_result = stmtObj1.executeUpdate(createFaveQuery);
                System.out.println("Insert count : "+insert_result);
        	}
        	if(update_result>0 || insert_result>0){
        		resMap.put("message", "Favourite route created.");
	    		resMap.put("statusCode", "200");

        	}else{
        		resMap.put("message", "already favourite");
	    		resMap.put("statusCode", "200");

        	}
        	
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: markFavourite "+sqlException.getMessage());
            operation=false;
            resMap.put("message", sqlException.getMessage());
    		resMap.put("statusCode", "500");
            
        }
        System.out.println("resMap : "+resMap);
        return resMap;
    }
    @SuppressWarnings("unused")
	public Map<String,String> getPassengerFav(MarkReqest markObj,
			DataSource dataSource,
			Connection connObj){

        ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        //ConnectionPool jdbcObj = new ConnectionPool();
        String passenger_id=null;
		Map<String,String> resMap=new HashMap<String,String>();

        try {

            // Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for getPassengerFav=====\n");
                connObj = dataSource.getConnection();
        	}

            String getPassengerFavQuery="select id,state from favourite "
            		+ " where passenger_id="+markObj.getPassenger_id()
            		+ " and route_id ="+markObj.getRoute_id();
            System.out.println("getPassengerFavQuery Query = "+getPassengerFavQuery);

            pstmtObj = connObj.prepareStatement(getPassengerFavQuery);

            rsObj = pstmtObj.executeQuery();
            while (rsObj.next()) {

                resMap.put("id", rsObj.getString("id"));
                resMap.put("state", rsObj.getString("state"));
                System.out.println("resMap : "+resMap);
            }
            
        } catch(Exception sqlException) {
            System.out.println("Exception  at: getPassengerFav "+sqlException.getMessage());
            sqlException.printStackTrace();
        }
        return resMap;
    }
    
    @SuppressWarnings("unused")
	public Map<String,String> removeFavourite(MarkReqest markObj,
		DataSource dataSource,
		Connection connObj){
    	ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result=0;
        int insert_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        Map<String,String> resMap=new HashMap<String, String>();
        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for removeFavourite=====\n");
                connObj = dataSource.getConnection();
        	}
        	Helper helperObj=new Helper();
        	System.out.println("markObj.getPassenger_id() :"+markObj.getPassenger_id());
        	String upFaveQuery=null;
        	FrameQuery frameObj=new FrameQuery();
        	String removeFavQuery=frameObj.getRemoveSelectedFavRouteQuery(markObj);
        	if(removeFavQuery !=null && !removeFavQuery.equals("")){
        		//Update fav 
        		stmtObj = connObj.createStatement();
                update_result = stmtObj.executeUpdate(removeFavQuery);
                System.out.println("Update count : "+update_result);
        	}
        	if(update_result>0){
        		System.out.println("\nFavourite route removed.");
        		resMap.put("message", "Favourite route removed.");
	    		resMap.put("statusCode", "200");
        	}else{
        		resMap.put("message", "Route does not exits.");
	    		resMap.put("statusCode", "400");
        	}
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: removeFavourite "+sqlException.getMessage());
            resMap.put("message", sqlException.getMessage());
    		resMap.put("statusCode", "500");
        }
        return resMap;
    }
    @SuppressWarnings("unused")
	public Map<String,String> suggestedRoute(SuggestedRouteRequest suggestedRouted,
			DataSource dataSource,
			Connection connObj){

    	ResultSet rsObj = null;
        //Connection connObj = null;
        PreparedStatement pstmtObj = null;
        Statement stmtObj = null;
        Statement stmtObj1 = null;
        int update_result=0;
        int insert_result=0;
        //ConnectionPool jdbcObj = new ConnectionPool();
        boolean operation=false;
        Map<String,String> resMap=new HashMap<String, String>();

        try {
        	// Performing Database Operation!
        	System.out.println("\n connObj ="+connObj+" isClosed: "+connObj.isClosed());
        	if(connObj ==null || connObj.isClosed()){
        		System.out.println("\n=====Making A New Connection for suggestedRoute =====\n");
                connObj = dataSource.getConnection();
        	}
        	Helper helperObj=new Helper();
        	System.out.println("suggestedRouted.getPassenger_id() :"+suggestedRouted.getPassenger_id());
        	
        	if(suggestedRouted.getPassenger_id()!=null && !suggestedRouted.getPassenger_id().equals("")){
        		String upFaveQuery=null;
            	FrameQuery frameObj=new FrameQuery();
            	String suggestedRouteQuery=frameObj.getSuggestedRouteQuery(suggestedRouted);
            	if(suggestedRouteQuery !=null && !suggestedRouteQuery.equals("")){
            		//create suggested route
            		stmtObj = connObj.createStatement();
            		insert_result = stmtObj.executeUpdate(suggestedRouteQuery);
                    System.out.println("Insert count : "+insert_result);
            	}
        	}
        	
        	if(insert_result>0){
        		operation=true;
        		System.out.println("\nSuggested route created.");
        		resMap.put("message", "Suggested route created.");
	    		resMap.put("statusCode", "200");
        	}else {
        		resMap.put("message", "Suggested route creation failed.");
	    		resMap.put("statusCode", "500");
        	}
        	
        } catch(Exception sqlException) {
            System.err.println("Exception  at: suggestedRoute "+sqlException.getMessage());
            resMap.put("message", sqlException.getMessage());
    		resMap.put("statusCode", "500");
        }
        System.out.println("resMap : "+resMap);
        return resMap;
    }
}

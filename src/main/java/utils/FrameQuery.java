package utils;

import config.ShuttleConfig;
import request.BookingRequest;
import request.CancelBookingRequest;
import request.DeviceInfoRequest;
import request.MarkReqest;
import request.PaytmChecksumVarificationRequest;
import request.PickUpByPassengerRequest;
import request.SuggestedRouteRequest;

public class FrameQuery {
	public  String getMarkFavQuery(MarkReqest markObj){
		StringBuilder createFavRecordQuery = new StringBuilder();
		createFavRecordQuery.append("insert into favourite (passenger_id,route_id,state");
		if(markObj.getRoute_timings_id()!=null && !markObj.getRoute_timings_id().equals("")){
			createFavRecordQuery.append(",route_timings_id");
		}
		if(markObj.getPick_stop_id()!=null && !markObj.getPick_stop_id().equals("")){
			createFavRecordQuery.append(",pick_stop_id");
		}
		if(markObj.getDrop_stop_id()!=null && !markObj.getDrop_stop_id().equals("")){
			createFavRecordQuery.append(",drop_stop_id");
		}
		createFavRecordQuery.append(",created_by,created_at,last_modified_at) values ");
		createFavRecordQuery.append("( "+markObj.getPassenger_id()+","+markObj.getRoute_id()+","+ShuttleConfig.FAVOURITE);
		if(markObj.getRoute_timings_id()!=null && !markObj.getRoute_timings_id().equals("")){
			createFavRecordQuery.append(","+markObj.getRoute_timings_id());
		}
		if(markObj.getPick_stop_id()!=null && !markObj.getPick_stop_id().equals("")){
			createFavRecordQuery.append(","+markObj.getPick_stop_id());
		}
		if(markObj.getDrop_stop_id()!=null && !markObj.getDrop_stop_id().equals("")){
			createFavRecordQuery.append(","+markObj.getDrop_stop_id());
		}
		createFavRecordQuery.append(","+markObj.getPassenger_id()+",now(),now() )");
		System.out.println("createFavRecordQuery : "+createFavRecordQuery.toString());
		return createFavRecordQuery.toString();
	}
	
	public  String getRemoveFavQuery(MarkReqest markObj){
		String removeFavQuery="update favourite set state="+ShuttleConfig.REMOVED_FROM_FAVOURITE+" "
				+ " where passenger_id="+markObj.getPassenger_id()+"";
		
		System.out.println("removeFavQuery : "+removeFavQuery);
		return removeFavQuery;
	}
	
	public  String getRemoveFavToFavQuery(MarkReqest markObj){
		String removeFavQuery="update favourite set state="+ShuttleConfig.FAVOURITE+" "
				+ " where passenger_id="+markObj.getPassenger_id()+" "
				+ " and route_id='"+markObj.getRoute_id()+"'";
		
		System.out.println("removeFavQuery : "+removeFavQuery);
		return removeFavQuery;
	}
	
	public  String getRemoveSelectedFavRouteQuery(MarkReqest markObj){
		String removeSelectedFavQuery="update favourite set "
				+ " state="+ShuttleConfig.REMOVED_FROM_FAVOURITE+", "
				+ " last_modified_at=now() "
				+ " where passenger_id="+markObj.getPassenger_id()+" "
				+ " and route_id="+markObj.getRoute_id();
		
		System.out.println("removeSelectedFavQuery : "+removeSelectedFavQuery);
		return removeSelectedFavQuery;
	}
	public  String getSuggestedRouteQuery(SuggestedRouteRequest suggestedRouted){
		StringBuilder createSuggestedQuery = new StringBuilder();
		createSuggestedQuery.append("insert into suggested_route (created_by");
		if(suggestedRouted.getPassenger_id()!=null && !suggestedRouted.getPassenger_id().equals("")){
			createSuggestedQuery.append(",passenger_id");
		}
		if(suggestedRouted.getPick()!=null && !suggestedRouted.getPick().equals("")){
			createSuggestedQuery.append(",fav_pick");
		}
		if(suggestedRouted.getDrop()!=null && !suggestedRouted.getDrop().equals("")){
			createSuggestedQuery.append(",fav_drop");
		}
		createSuggestedQuery.append(",frequency,created_at,last_modified_at,last_modified_by) values ("+suggestedRouted.getPassenger_id()+"");
		if(suggestedRouted.getPassenger_id()!=null && !suggestedRouted.getPassenger_id().equals("")){
			createSuggestedQuery.append(","+suggestedRouted.getPassenger_id()+"");
		}
		if(suggestedRouted.getPick()!=null && !suggestedRouted.getPick().equals("")){
			createSuggestedQuery.append(",'"+suggestedRouted.getPick()+"'");
		}
		if(suggestedRouted.getDrop()!=null && !suggestedRouted.getDrop().equals("")){
			createSuggestedQuery.append(",'"+suggestedRouted.getDrop()+"'");
		}
		createSuggestedQuery.append(","+suggestedRouted.getFrequency()+",now(),now(),"+suggestedRouted.getPassenger_id()+")");
		System.out.println("createSuggestedQuery : "+createSuggestedQuery);
		return createSuggestedQuery.toString();
	}
	
	public  String getBookingSeatQuery(BookingRequest bookingObj,String booking_number){
		StringBuilder createBookingSeatQuery = new StringBuilder();
		createBookingSeatQuery.append("insert into booking (passenger_id,booking_number,trip_id,pick_stop_id,drop_stop_id,state,fare");
		createBookingSeatQuery.append(",created_by,created_at,last_modified_by,last_modified_at) values ");
		createBookingSeatQuery.append("("+bookingObj.getPassenger_id()+",'"+booking_number+"',"+bookingObj.getTrip_id());
		createBookingSeatQuery.append(","+bookingObj.getPick_stop_id()+","+bookingObj.getDrop_stop_id());
		createBookingSeatQuery.append(","+ShuttleConfig.BOOKING_CONFIRMED+",(select fare from trip where id="+bookingObj.getTrip_id()+")");
		createBookingSeatQuery.append(","+bookingObj.getPassenger_id()+",now(),"+bookingObj.getPassenger_id()+",now())");

		System.out.println("createBookingSeatQuery : "+createBookingSeatQuery);
		return createBookingSeatQuery.toString();
	}
	
	public  String getCancelBookingQuery(CancelBookingRequest cancelBookReqObj){
		String getCancelBookingQ="update booking set state='"+ShuttleConfig.BOOKING_CANCEL+"' "
				+ " where id='"+cancelBookReqObj.getBooking_id()+"' ";
		System.out.println("getCancelBookingQuery : "+getCancelBookingQ);
		return getCancelBookingQ;
	}
	
	public  String getPassengerTripCancelQuery(CancelBookingRequest cancelBookReqObj){
		String getPassengerTripCancelQ="update passenger_trip set state='"+ShuttleConfig.PASSENGER_TRIP_CANCELLED+"' "
				+ " where passenger_id="+cancelBookReqObj.getPassenger_id()+""
				+ " and trip_id="+cancelBookReqObj.getTrip_id()+" "
				+ " and state='"+ShuttleConfig.PASSENGER_TRIP_CREATED+"' ";
		System.out.println("getPassengerTripCancelQuery : "+getPassengerTripCancelQ);
		return getPassengerTripCancelQ;
	}
	
	
	public  String updateTripSeatQuery(BookingRequest bookingObj){
		StringBuilder updateTripSeatQuery = new StringBuilder();
		updateTripSeatQuery.append("update trip set remaining_seats = (remaining_seats - "+bookingObj.getNumber_of_seat()+") ");
		updateTripSeatQuery.append(" where total_seats >= remaining_seats ");
		updateTripSeatQuery.append(" and remaining_seats >=  "+bookingObj.getNumber_of_seat()+" ");
		updateTripSeatQuery.append(" and id =  "+bookingObj.getTrip_id()+" ");

		System.out.println("updateTripSeatQuery : "+updateTripSeatQuery);
		return updateTripSeatQuery.toString();
	}
	
	public  String getIncrementTripSeatQuery(CancelBookingRequest cancelBookReqObj){
		StringBuilder updateTripSeatQuery = new StringBuilder();
		updateTripSeatQuery.append("update trip set remaining_seats = (remaining_seats + 1) ");
		updateTripSeatQuery.append(" where remaining_seats <= total_seats ");
		updateTripSeatQuery.append(" and id =  "+cancelBookReqObj.getTrip_id()+" ");

		System.out.println("updateTripSeatQuery : "+updateTripSeatQuery);
		return updateTripSeatQuery.toString();
	}
	
	public String insertTransactionRecordQuery(){
		  StringBuilder insertTransactionQuery = new StringBuilder();
		  insertTransactionQuery.append("insert into txn (booking_uuid,customer_mob,txn_amount,txn_date,");
		  insertTransactionQuery.append("created_by,created_at,last_modified_by,last_modified_at) values ");
		  insertTransactionQuery.append("(?,?,?,?,?,?,?,?)");
		  System.out.println("insertTransactionRecordQuery : "+insertTransactionQuery);
		  return insertTransactionQuery.toString();
	}
	
	public String insertPaymentRecordQuery(){
		StringBuilder insertPaymentQuery = new StringBuilder();
		insertPaymentQuery.append("insert into payment (amount,boooking_id,txn_id,state,is_paid,");
		insertPaymentQuery.append("created_by,created_at,last_modified_by,last_modified_at) values ");
		insertPaymentQuery.append("(?,?,?,?,?,?,?,?,?)");
		System.out.println("insertPaymentQuery : "+insertPaymentQuery);
		return insertPaymentQuery.toString();
	}
	
	public String updateTransactionRecordQuery(PaytmChecksumVarificationRequest ChecksumVarificationParameter){
		  StringBuilder updateTransactionQuery = new StringBuilder();
		  updateTransactionQuery.append("update txn set ext_id="+ChecksumVarificationParameter.getTxnId()+",");
		  updateTransactionQuery.append("status="+ChecksumVarificationParameter.getStatus()+",");
		  //updateTripSeatQuery.append("gateway="+ChecksumVarificationParameter.get+",");
		  //updateTripSeatQuery.append("gateway_res="+ChecksumVarificationParameter.get+",");
		  updateTransactionQuery.append("bank_name="+ChecksumVarificationParameter.getBankName()+",");
		  updateTransactionQuery.append("txn_date="+ChecksumVarificationParameter.getStatus()+",");
		  updateTransactionQuery.append("booking_uuid="+Integer.parseInt(ChecksumVarificationParameter.getOrderId())+",");
		  updateTransactionQuery.append("txn_amount="+Double.parseDouble(ChecksumVarificationParameter.getTxnAmount())+",");
		  updateTransactionQuery.append("payment_mode="+ChecksumVarificationParameter.getPaymentMode()+",");
		  updateTransactionQuery.append("bank_txn_id="+ChecksumVarificationParameter.getBankTxnId()+",");
		  updateTransactionQuery.append("currency="+ChecksumVarificationParameter.getCurrency()+",");
		  updateTransactionQuery.append("gateway_name="+ChecksumVarificationParameter.getGatewayName()+",");
		  updateTransactionQuery.append("resp_msg="+ChecksumVarificationParameter.getRespMsg()+",");
		  updateTransactionQuery.append("resp_code="+ChecksumVarificationParameter.getRespCode()+",");
		  //updateTripSeatQuery.append("last_modified_by="+ChecksumVarificationParameter.+",");
		  updateTransactionQuery.append("last_modified_at=now()");
		  updateTransactionQuery.append(" where booking_uuid="+Integer.parseInt(ChecksumVarificationParameter.getOrderId()));

		  System.out.println("updateTransactionQuery : "+updateTransactionQuery);
		  return updateTransactionQuery.toString();
		 }
	
	public String updatePaymentRecordQuery(PaytmChecksumVarificationRequest ChecksumVarificationParameter) {
		  int booking_id = Integer.parseInt(ChecksumVarificationParameter.getBookingid());
		  boolean is_paid=false;
		  if(ChecksumVarificationParameter.getRespCode()=="01") {
		   is_paid = true;
		  }
		  else {
		    is_paid = false;
		  }
		  StringBuilder updatePaymentQuery = new StringBuilder();
		  updatePaymentQuery.append("update payment set state="+Integer.parseInt(ShuttleConfig.PAYMENT_COMPLETED)+",is_paid="+is_paid+",");
		  updatePaymentQuery.append("last_modified_at=now()");
		  updatePaymentQuery.append("where booking_id="+booking_id);
		  return updatePaymentQuery.toString();
	}
	
	public String getInsertNotifQuery(String email,String msgBody,String state,String notif_type){
		StringBuilder getInsertSmsQ = new StringBuilder();
		getInsertSmsQ.append("insert into notif (user_id,msg,state,notif_type,created_date)");
		getInsertSmsQ.append(" values ");
		getInsertSmsQ.append("((select id from usr where email='"+email+"'),'"+msgBody+"','"+state+"','"+notif_type+"',now())");
		System.out.println("getInsertSmsQ : "+getInsertSmsQ);
		return getInsertSmsQ.toString();
		
	}
	
	public String getDeviceInsertQuery(DeviceInfoRequest deviceUpdateObj){
		StringBuilder deviceInsertQuery = new StringBuilder();
		deviceInsertQuery.append("insert into device (created_by");
		if(deviceUpdateObj.getUser_id()!=null && !deviceUpdateObj.getUser_id().equals("")){
			deviceInsertQuery.append(",user_id");
		}
		if(deviceUpdateObj.getImei1()!=null && !deviceUpdateObj.getImei1().equals("")){
			deviceInsertQuery.append(",imei1");
		}
		if(deviceUpdateObj.getImei2()!=null && !deviceUpdateObj.getImei2().equals("")){
			deviceInsertQuery.append(",imei2");
		}
		if(deviceUpdateObj.getPush_token()!=null && !deviceUpdateObj.getPush_token().equals("")){
			deviceInsertQuery.append(",push_token");
		}
		if(deviceUpdateObj.getDevice_uid1()!=null && !deviceUpdateObj.getDevice_uid1().equals("")){
			deviceInsertQuery.append(",device_uid1");
		}
		if(deviceUpdateObj.getDevice_uid2()!=null && !deviceUpdateObj.getDevice_uid2().equals("")){
			deviceInsertQuery.append(",device_uid2");
		}
		if(deviceUpdateObj.getOs_type()!=null && !deviceUpdateObj.getOs_type().equals("")){
			deviceInsertQuery.append(",os_type");
		}
		if(deviceUpdateObj.getOs_version()!=null && !deviceUpdateObj.getOs_version().equals("")){
			deviceInsertQuery.append(",os_version");
		}
		if(deviceUpdateObj.getApp_version()!=null && !deviceUpdateObj.getApp_version().equals("")){
			deviceInsertQuery.append(",app_version");
		}
		if(deviceUpdateObj.getModel()!=null && !deviceUpdateObj.getModel().equals("")){
			deviceInsertQuery.append(",model");
		}
		if(deviceUpdateObj.getCarrier()!=null && !deviceUpdateObj.getCarrier().equals("")){
			deviceInsertQuery.append(",carrier");
		}
		
		deviceInsertQuery.append(",status,created_at,last_modified_at,last_modified_by) values ("+deviceUpdateObj.getUser_id()+"");
		
		if(deviceUpdateObj.getUser_id()!=null && !deviceUpdateObj.getUser_id().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getUser_id()+"'");
		}
		if(deviceUpdateObj.getImei1()!=null && !deviceUpdateObj.getImei1().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getImei1()+"'");
		}
		if(deviceUpdateObj.getImei2()!=null && !deviceUpdateObj.getImei2().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getImei2()+"'");
		}
		if(deviceUpdateObj.getPush_token()!=null && !deviceUpdateObj.getPush_token().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getPush_token()+"'");
		}
		if(deviceUpdateObj.getDevice_uid1()!=null && !deviceUpdateObj.getDevice_uid1().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getDevice_uid1()+"'");
		}
		if(deviceUpdateObj.getDevice_uid2()!=null && !deviceUpdateObj.getDevice_uid2().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getDevice_uid2()+"'");
		}
		if(deviceUpdateObj.getOs_type()!=null && !deviceUpdateObj.getOs_type().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getOs_type()+"'");
		}
		if(deviceUpdateObj.getOs_version()!=null && !deviceUpdateObj.getOs_version().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getOs_version()+"'");
		}
		if(deviceUpdateObj.getApp_version()!=null && !deviceUpdateObj.getApp_version().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getApp_version()+"'");
		}
		if(deviceUpdateObj.getModel()!=null && !deviceUpdateObj.getModel().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getModel()+"'");
		}
		if(deviceUpdateObj.getCarrier()!=null && !deviceUpdateObj.getCarrier().equals("")){
			deviceInsertQuery.append(",'"+deviceUpdateObj.getCarrier()+"'");
		}
		deviceInsertQuery.append(",'active',now(),now(),"+deviceUpdateObj.getUser_id()+")");
		System.out.println("deviceInsertQuery : "+deviceInsertQuery);
		return deviceInsertQuery.toString();
	}
	
	public String getDeviceUpdateQuery(DeviceInfoRequest deviceUpdateObj){

		StringBuilder updateDeviceQuery = new StringBuilder();
		updateDeviceQuery.append("update device set "
	 			+" last_modified_at='now()', ");
		
		
		if(deviceUpdateObj.getImei1()!=null && !deviceUpdateObj.getImei1().equals("")){
			updateDeviceQuery.append(" imei1= '"+deviceUpdateObj.getImei1()+"',");
		}
		if(deviceUpdateObj.getImei2()!=null && !deviceUpdateObj.getImei2().equals("")){
			updateDeviceQuery.append(" imei2= '"+deviceUpdateObj.getImei2()+"',");
		}
		if(deviceUpdateObj.getPush_token()!=null && !deviceUpdateObj.getPush_token().equals("")){
			updateDeviceQuery.append(" push_token= '"+deviceUpdateObj.getPush_token()+"',");
		}
		if(deviceUpdateObj.getDevice_uid1()!=null && !deviceUpdateObj.getDevice_uid1().equals("")){
			updateDeviceQuery.append(" device_uid1= '"+deviceUpdateObj.getDevice_uid1()+"',");
		}
		if(deviceUpdateObj.getDevice_uid2()!=null && !deviceUpdateObj.getDevice_uid2().equals("")){
			updateDeviceQuery.append(" device_uid2= '"+deviceUpdateObj.getDevice_uid2()+"',");
		}
		if(deviceUpdateObj.getOs_type()!=null && !deviceUpdateObj.getOs_type().equals("")){
			updateDeviceQuery.append(" os_type= '"+deviceUpdateObj.getOs_type()+"',");
		}
		if(deviceUpdateObj.getOs_version()!=null && !deviceUpdateObj.getOs_version().equals("")){
			updateDeviceQuery.append(" os_version= '"+deviceUpdateObj.getOs_version()+"',");
		}
		if(deviceUpdateObj.getApp_version()!=null && !deviceUpdateObj.getApp_version().equals("")){
			updateDeviceQuery.append(" app_version= '"+deviceUpdateObj.getApp_version()+"',");
		}
		if(deviceUpdateObj.getModel()!=null && !deviceUpdateObj.getModel().equals("")){
			updateDeviceQuery.append(" model= '"+deviceUpdateObj.getModel()+"',");
		}
		if(deviceUpdateObj.getCarrier()!=null && !deviceUpdateObj.getCarrier().equals("")){
			updateDeviceQuery.append(" carrier= '"+deviceUpdateObj.getCarrier()+"',");
		}
		updateDeviceQuery.append(" status= 'active' where user_id='"+deviceUpdateObj.getUser_id()+"' ");

		
		
		System.out.println("updateDeviceQuery : "+updateDeviceQuery);
		return updateDeviceQuery.toString();
	
		
	}
	
	public String getCreatePassengerTripQuery(BookingRequest bookingObj){
		String getCreatePassengerTripQ="insert into passenger_trip (passenger_id,pick_stop_id,drop_stop_id,state,trip_id,fare) "
				+ "values "
				+ "("+bookingObj.getPassenger_id()+","+bookingObj.getPick_stop_id()+","
				+ ""+bookingObj.getDrop_stop_id()+","+ShuttleConfig.BOOKING_INITIATED+","+bookingObj.getTrip_id()+","
				+ "(select fare from trip where id="+bookingObj.getTrip_id()+"))";
		System.out.println("getCreatePassengerTripQ : "+getCreatePassengerTripQ);
		return getCreatePassengerTripQ;
		
	}
	
	public  String getUpdateBookingQuery(PickUpByPassengerRequest pickUpPassReqObj){
		String getUpdateBookingQ="update booking set state='"+ShuttleConfig.PICKUP_CONFIRMED_FOR_BOOKING+"' "
				+ " where id="+pickUpPassReqObj.getBooking_id()+" ";
		System.out.println("getUpdateBookingQ Query : "+getUpdateBookingQ);
		return getUpdateBookingQ;
	}
	
	public  String getPassengerTripPickUpQuery(PickUpByPassengerRequest pickUpPassReqObj){
		String getPassengerTripPickUpQueryQ="update passenger_trip set state='"+ShuttleConfig.PASSENGER_PICKED_UP+"',pick_time=now() "
				+ " where passenger_id="+pickUpPassReqObj.getPassenger_id()+""
				+ " and trip_id="+pickUpPassReqObj.getTrip_id()+" "
				+ " and state='"+ShuttleConfig.PASSENGER_TRIP_CREATED+"' ";
		System.out.println("getPassengerTripPickUpQueryQ : "+getPassengerTripPickUpQueryQ);
		return getPassengerTripPickUpQueryQ;
	}
	
	
	

}

package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class SeatMapper {
	public List<String> getSeatList(Map<String,String> seatMap){
		int trip_id=Integer.parseInt(seatMap.get("trip_id"));
		int total_seats=Integer.parseInt(seatMap.get("total_seats"));
		int remaining_seats=Integer.parseInt(seatMap.get("remaining_seats"));
		List<String> seatList=new ArrayList<String>();
		for(int i=0;i<remaining_seats;i++){
			//System.out.println("seat_"+total_seats+"****"+seatMap.get("license_plate")+"_seat_"+total_seats);
			seatList.add("license_plate_"+seatMap.get("license_plate")+"_trip_id_"+trip_id+"_seat_"+total_seats);
			
			total_seats=total_seats-1;
		}
		Collections.reverse(seatList);
		System.out.println("seatList"+seatList);
		return seatList;
	}
	
}

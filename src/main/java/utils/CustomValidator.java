package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomValidator {
	
	public static boolean isEmailValid(String email) {
		boolean isValid = false;
		//Initialize reg ex for email.
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;
		//Make the comparison case-insensitive.
		Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if(matcher.matches()) {
			isValid = true;
		}
		System.out.println(""+email+" validation result :"+isValid);
		return isValid;
	}

	public static boolean isPhoneNumberValid(String phoneNumber) {
		boolean isValid = false;
		//Initialize reg ex for phone number. 
		String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
		CharSequence inputStr = phoneNumber;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if(matcher.matches()) {
			isValid = true;
		}
		System.out.println(""+phoneNumber+" validation result :"+isValid);
		return isValid;
	}
	public static String getValidDecimalNumber(String decimalNumber) {
		try {
			String decimalNumber_arr[] = decimalNumber.split("\\.");
			System.out.println("decimalNumber_arr[0]="+decimalNumber_arr[0]+"\ndecimalNumber_arr[1]="+decimalNumber_arr[1]+""
					+ "\ndecimalNumber_arr[0].length="+decimalNumber_arr[0].length()+"\ndecimalNumber_arr[1].length="
					+ decimalNumber_arr[1].length());
			String fnum=decimalNumber_arr[0];
			String lnum=decimalNumber_arr[1];
			String strNumber=null;
			boolean isDecimalNumberFormated=false;
			if(fnum.length()>3) {
				isDecimalNumberFormated=true;
				String f_Num=fnum.substring((fnum.length()-3), fnum.length());
				String L_Num="";
				String DNum="";
				if(lnum.length()>6) {
					L_Num=lnum.substring(0, 6);
					DNum=f_Num+"."+L_Num;
				} else {
					DNum= f_Num+"."+lnum;
				}
				strNumber=DNum;
			} else if(lnum.length()>6) {
				isDecimalNumberFormated=true;
				strNumber=fnum+"."+lnum.substring(0, 6);
			}
			System.out.println("is decimalNumber formated="+isDecimalNumberFormated);
			if(isDecimalNumberFormated) {
				System.out.println("Actual Number="+decimalNumber + " Changed to="+strNumber );
				decimalNumber=strNumber;
			} else {
				System.out.println("Actual Number="+decimalNumber + " Number not formated="+isDecimalNumberFormated);
			}
		} catch(Exception e) {
			System.err.println("Exception while getValidDecimalNumber Actual Number="+decimalNumber +" and Exception="+e.getMessage());
			decimalNumber="0.00";
		}
		return decimalNumber;
	}
}


package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import request.MarkReqest;

public class ConvertJsonToObject {
	
	public MarkReqest markRequest(InputStream inputStream) {
		
		MarkReqest markObj=new MarkReqest();
		try {
			JSONParser parser = new JSONParser();
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			JSONObject event = (JSONObject)parser.parse(reader);
			if (event.get("body") != null) {
				JSONObject body = (JSONObject)parser.parse((String)event.get("body"));
				System.out.println("body : "+body);
				if ( body.get("user_id") != null) {
					Object user_id=body.get("user_id");
					System.out.println("user : "+String.valueOf(user_id));
					markObj.setUser_id(String.valueOf(user_id));
				}
				if ( body.get("route_id") != null) {
					//markObj.setState((String)body.get("route_id")) ;
				}
				if ( body.get("route_timings_id") != null) {
					//markObj.setState((String)body.get("route_timings_id")) ;
				}
				if ( body.get("pick_stop_id") != null) {
					//markObj.setState((String)body.get("pick_stop_id")) ;
				}
				if ( body.get("drop_stop_id") != null) {
					//markObj.setState((String)body.get("drop_stop_id")) ;
				}
				if ( body.get("state") != null) {
					//markObj.setState((String)body.get("state")) ;
				}
            }else {
        	 System.out.println("body does not exits.");
            }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Exception : "+e.getMessage());
		}
		return markObj;
	}

}

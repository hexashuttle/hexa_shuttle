package utils;

import java.util.UUID;

public class CustomeHelper {
	
	public static String getBookingNumber(){
		// Creating a random UUID (Universally unique identifier).
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString().replace("-", "").toUpperCase();
        System.out.println("randomUUIDString = "+randomUUIDString);
        return randomUUIDString;
	}

}

package utils;

import java.util.Map;
import org.apache.commons.lang.StringEscapeUtils;


public class NotificationBody {
	
	public static String getBookingNotifBodyForDb(Map<String,String> resMap,String raw_body){
		
		String configured_body = raw_body.replace("'", "''");
        System.out.println("getBookingNotifBodyForDb body : "+configured_body);
		String booking_number=resMap.get("booking_number");
		System.out.println("booking_number : "+booking_number);
		if (booking_number != null && booking_number.length() >9) {
		  
			booking_number = booking_number.substring(booking_number.length() - 9);
		}
		String actual_body=configured_body.replace("@booking_number", booking_number.toUpperCase());
		System.out.println("getBookingNotifBodyForDb actual_body : "+actual_body);
		
		return actual_body;
	}
	public static String getBookingNotifBodyForMsg(Map<String,String> resMap,String raw_body){
        System.out.println("configured_body : "+raw_body);
		String booking_number=resMap.get("booking_number");
		if (booking_number != null && booking_number.length() >9) {
		  
			booking_number = booking_number.substring(booking_number.length() - 9);
		}
		String actual_body=raw_body.replace("@booking_number", booking_number.toUpperCase());
		System.out.println("getBookingNotifBodyForMsg actual_body : "+actual_body);
		
		return actual_body;
	}
	
	public static String getCancelBookingNotifBodyForDb(Map<String,String> resMap,String raw_body){
		
		String configured_body = raw_body.replace("'", "''");
        System.out.println("getCancelBookingNotifBodyForDb body : "+configured_body);
		String booking_number=resMap.get("booking_number");
		System.out.println("booking_number : "+booking_number);
		if (booking_number != null && booking_number.length() >9) {
		  
			booking_number = booking_number.substring(booking_number.length() - 9);
		}
		String actual_body=configured_body.replace("@booking_number", booking_number.toUpperCase());
		System.out.println("getBookingNotifBodyForDb actual_body : "+actual_body);
		
		return actual_body;
	}
	public static String getCancelBookingNotifBodyForMsg(Map<String,String> resMap,String raw_body){
        System.out.println("getCancelBookingNotifBodyForMsg : "+raw_body);
		String booking_number=resMap.get("booking_number");
		if (booking_number != null && booking_number.length() >9) {
		  
			booking_number = booking_number.substring(booking_number.length() - 9);
		}
		String actual_body=raw_body.replace("@booking_number", booking_number.toUpperCase());
		System.out.println("getBookingNotifBodyForMsg actual_body : "+actual_body);
		
		return actual_body;
	}

}

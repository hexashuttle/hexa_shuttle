package utils;

public class UserConfigMapper {
	public int arrival_notif;
	public boolean is_sms;
	public boolean is_push;
	public boolean is_mail;
	public boolean is_voice;
	
	public int getArrival_notif() {
		return arrival_notif;
	}
	public void setArrival_notif(int arrival_notif) {
		this.arrival_notif = arrival_notif;
	}
	public boolean isIs_sms() {
		return is_sms;
	}
	public void setIs_sms(boolean is_sms) {
		this.is_sms = is_sms;
	}
	public boolean isIs_push() {
		return is_push;
	}
	public void setIs_push(boolean is_push) {
		this.is_push = is_push;
	}
	public boolean isIs_mail() {
		return is_mail;
	}
	public void setIs_mail(boolean is_mail) {
		this.is_mail = is_mail;
	}
	public boolean isIs_voice() {
		return is_voice;
	}
	public void setIs_voice(boolean is_voice) {
		this.is_voice = is_voice;
	}
}

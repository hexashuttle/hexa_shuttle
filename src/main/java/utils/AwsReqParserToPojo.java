package utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import request.BookingRequest;
import request.CancelBookingRequest;
import request.DeviceInfoRequest;
import request.MarkReqest;
import request.PickUpByPassengerRequest;
import request.SuggestedRouteRequest;

public class AwsReqParserToPojo {
	
	public BookingRequest getBookingRequest(String body){
		System.out.println(" getBookingRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		BookingRequest bookReqObj=new BookingRequest();
		try{
			bookReqObj = mapper.readValue(body, BookingRequest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return bookReqObj;
	}
	
	public MarkReqest getMarkReqestRequest(String body){
		System.out.println("getMarkReqestRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		MarkReqest markReqObj=new MarkReqest();
		try{
			markReqObj = mapper.readValue(body, MarkReqest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return markReqObj;
	}
	
	public SuggestedRouteRequest getSuggestedRouteRequest(String body){
		System.out.println("getSuggestedRouteRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		SuggestedRouteRequest suggestedRouteObj=new SuggestedRouteRequest();
		try{
			suggestedRouteObj = mapper.readValue(body, SuggestedRouteRequest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return suggestedRouteObj;
	}
	
	public CancelBookingRequest getCancelBookingRequest(String body){
		System.out.println("getCancelBookingRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		CancelBookingRequest cancelBookReqObj=new CancelBookingRequest();
		try{
			cancelBookReqObj = mapper.readValue(body, CancelBookingRequest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return cancelBookReqObj;
	}
	
	public DeviceInfoRequest getDeviceInfoRequest(String body){
		System.out.println("getDeviceInfoRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		DeviceInfoRequest deviceUpdateObj=new DeviceInfoRequest();
		try{
			deviceUpdateObj = mapper.readValue(body, DeviceInfoRequest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return deviceUpdateObj;
	}
	
	public PickUpByPassengerRequest getPickUpByPassengerRequest(String body){
		System.out.println("getPickUpByPassengerRequest body : "+body);
		ObjectMapper mapper = new ObjectMapper();
		PickUpByPassengerRequest pickUpPassReqObj=new PickUpByPassengerRequest();
		try{
			pickUpPassReqObj = mapper.readValue(body, PickUpByPassengerRequest.class);
		}catch(Exception e){
			System.err.println("Exception while json parsing.."+e.getMessage());
		}
		return pickUpPassReqObj;
	}

}

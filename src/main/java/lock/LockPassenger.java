package lock;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

//import org.junit.Test;
import com.amazonaws.services.dynamodbv2.AcquireLockOptions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBLockClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBLockClientOptions;
import com.amazonaws.services.dynamodbv2.LockItem;

import config.ShuttleConfig;
import controller.SendNotif;
import db.operartion.Booking;
import request.BookingRequest;


public class LockPassenger {
	
    public Map<String,String> createLockAndBookSeat(String key,
    		BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj) throws InterruptedException, IOException {

    	boolean result=false;
    	Map<String,String> resMap=new HashMap<String,String>();
    	try {
    		// Inject client configuration to the builder like the endpoint and signing region
        	final AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard()
            		.withRegion("ap-southeast-1")
            		.build();
            // Whether or not to create a heartbeating background thread
            final boolean createHeartbeatBackgroundThread = false;
            Long leaseDuration=120L;
            //build the lock client
            final AmazonDynamoDBLockClient client = new AmazonDynamoDBLockClient(
                AmazonDynamoDBLockClientOptions.builder(dynamoDB, ShuttleConfig.TABLE_LOCK)
                        .withTimeUnit(TimeUnit.SECONDS)
                        .withLeaseDuration(leaseDuration)
                        //.withHeartbeatPeriod(3L)
                        .withCreateHeartbeatBackgroundThread(createHeartbeatBackgroundThread)
                        .withOwnerName(bookingObj.getPassenger_id())
                        .build());
            
            Optional<String> sortKey=Optional.empty();
			Optional<LockItem> lock = client.getLock(key, sortKey);
			boolean checkLockExits=lock.isPresent();
			System.out.println("checkLockExits : "+checkLockExits);
			if(checkLockExits){
				System.out.println("Lock Already Exits..");
				resMap.put("result","failed");
			}else{
				System.out.println("Lock Not Exits..Creating Lock..");
				//try to acquire a lock on the partition key "id"
		        final Optional<LockItem> lockItem = client.tryAcquireLock(AcquireLockOptions.builder(key).build());
		        System.out.println("lockItem.isPresent() : "+lockItem.isPresent());
		        if (lockItem.isPresent()) {
		        	System.out.println("Acquired lock! If I die, my lock will expire in "+leaseDuration+" seconds.");
		            //client.releaseLock(lockItem.get());
		            System.out.println("Lock Item Record : "+lockItem.get());
		            //Execute Business Logic.
		            Booking bookObj=new Booking();
		            resMap=bookObj.bookSeat(bookingObj, dataSource, connObj);
		            System.out.println("Seat Booking process : "+resMap);
		            //System.out.println("Release Lock : "+client.releaseLock(lockItem.get()));
		            //Clear all lock if seat booking faild
		            if(resMap!=null && !resMap.isEmpty()){
		        	   String action=resMap.get("result");
		        	   if(action!=null && action.equals("success")){
			            	SendNotif sendNotifObj=new SendNotif();
			            	sendNotifObj.invokeBookinNotification(resMap,bookingObj.getEmail(), dataSource, connObj);
			            }else {
			            	client.close();
			            }
		           }else{
		        	   client.close(); 
		           }
		           //lock_exits=true;
		           //lockProcessMap.put("lock_exits", lock_exits);
		        } else {
		            System.out.println("Failed to acquire lock!");
		        }
	    	}
			//client.close();
        }catch(Exception e){
    		System.err.println("Exception at lockingProcess : "+e.getMessage());
    		
    	}
    	System.out.println("resMap : "+resMap);
        return resMap;
    }
    
    public Map<String,String> lockingWhilePaymentInitiation(String key,
    		BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj) throws InterruptedException, IOException {
    	
    	boolean lock_exits=false;
		Map<String,String> bookingMap=new HashMap<String,String>();

    	try {
    		// Inject client configuration to the builder like the endpoint and signing region
        	final AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard()
            		.withRegion("ap-southeast-1")
            		.build();
            // Whether or not to create a heartbeating background thread
            //final boolean createHeartbeatBackgroundThread = false;
        	Long leaseDuration=120L;
            //build the lock client
            final AmazonDynamoDBLockClient client = new AmazonDynamoDBLockClient(
                AmazonDynamoDBLockClientOptions.builder(dynamoDB, ShuttleConfig.TABLE_LOCK)
                        .withTimeUnit(TimeUnit.SECONDS)
                        .withLeaseDuration(120L)
                        //.withHeartbeatPeriod(3L)
                        //.withCreateHeartbeatBackgroundThread(createHeartbeatBackgroundThread)
                        .withOwnerName(bookingObj.getPassenger_id())
                        .build());
            
            Optional<String> sortKey=Optional.empty();
			Optional<LockItem> lock = client.getLock(key, sortKey);
			boolean checkLockExits=lock.isPresent();
			System.out.println("checkLockExits : "+checkLockExits);
			if(checkLockExits){
				System.out.println("Lock Already Exits..");
				//client.releaseLock(lock.get());
			}else{
				System.out.println("Lock Not Exits.....Creating Lock.."+new Date().getMinutes()+" "+new Date().getSeconds());
				boolean isBookingAllowed=true;//decisionObj.isBookingAllowed(availableSeatMap);
	    		System.out.println("Seat Availability check in Loop.."+isBookingAllowed);
	    		if(isBookingAllowed){
	    			 //try to acquire a lock on the partition key "id"
		            final Optional<LockItem> lockItem = client.tryAcquireLock(AcquireLockOptions.builder(key).build());
		            System.out.println("lockItem.isPresent() : "+lockItem.isPresent());
		            if (lockItem.isPresent()) {
		                System.out.println("Acquired lock! If I die, my lock will expire in "+leaseDuration+" seconds.");
		                //client.releaseLock(lockItem.get());
		                System.out.println("Lock Item Record : "+lockItem.get());
		                //Execute Business Logic.
		                Booking bookObj=new Booking();
		                bookingMap=bookObj.initiateBookSeat(bookingObj, dataSource, connObj);
		                System.out.println("Release Lock : "+client.releaseLock(lockItem.get()));
		                lock_exits=true;
		                bookingMap.put("lock_exits", "true");
		            } else {
		                System.out.println("Failed to acquire lock!");
		            }
	    		}else{
	    			System.out.println("Seat Not Available..");
	    		}
			}
			//client.close();
        }catch(Exception e){
    		System.err.println("Exception at lockingProcess : "+e.getMessage());
    	}
        System.out.println("bookingMap in lockingWhilePaymentInitiation : "+bookingMap);
        return bookingMap;
    }
    
    public boolean lockingWhileSeatConfirmation(String key,
    		BookingRequest bookingObj,
			DataSource dataSource,
			Connection connObj) throws InterruptedException, IOException {

    	
    	boolean lock_exits=false;
    	try {
    		// Inject client configuration to the builder like the endpoint and signing region
        	final AmazonDynamoDB dynamoDB = AmazonDynamoDBClientBuilder.standard()
            		.withRegion("ap-southeast-1")
            		.build();
            // Whether or not to create a heartbeating background thread
            final boolean createHeartbeatBackgroundThread = false;
            //build the lock client
            Long leaseDuration=120L;
            final AmazonDynamoDBLockClient client = new AmazonDynamoDBLockClient(
                AmazonDynamoDBLockClientOptions.builder(dynamoDB, ShuttleConfig.TABLE_LOCK)
                        .withTimeUnit(TimeUnit.SECONDS)
                        .withLeaseDuration(leaseDuration)
                        //.withHeartbeatPeriod(3L)
                        .withCreateHeartbeatBackgroundThread(createHeartbeatBackgroundThread)
                        .withOwnerName(bookingObj.getPassenger_id())
                        .build());
            
            Optional<String> sortKey=Optional.empty();
			Optional<LockItem> lock = client.getLock(key, sortKey);
			boolean checkLockExits=lock.isPresent();
			System.out.println("checkLockExits : "+checkLockExits);
			if(checkLockExits){
				System.out.println("Lock Already Exits..");
				//client.releaseLock(lock.get());
			}else{
				System.out.println("Lock Not Exits..Creating Lock.."+new Date().getMinutes()+" "+new Date().getSeconds());
//				System.out.println("In For Loop..");
//				Helper helperObj=new Helper();
//		        Map<String,String> availableSeatMap=helperObj.getAvailableSeat(bookingObj, dataSource, connObj);
//		        TakeDecision decisionObj=new TakeDecision();
		        boolean isBookingAllowed=true;//decisionObj.isBookingAllowed(availableSeatMap);
	    		System.out.println("Seat Availability check in Loop.."+isBookingAllowed);
	    		if(isBookingAllowed){
	    			 //try to acquire a lock on the partition key "id"
		            final Optional<LockItem> lockItem = client.tryAcquireLock(AcquireLockOptions.builder(key).build());
		            System.out.println("lockItem.isPresent() : "+lockItem.isPresent());
		            if (lockItem.isPresent()) {
		                System.out.println("Acquired lock! If I die, my lock will expire in "+leaseDuration+" seconds.");
		                //client.releaseLock(lockItem.get());
		                System.out.println("Lock Item Record : "+lockItem.get());
		                //Execute Business Logic.
		                Booking bookObj=new Booking();
		                bookObj.bookSeat(bookingObj, dataSource, connObj);
		                //System.out.println("Release Lock : "+client.releaseLock(lockItem.get()));
		                lock_exits=true;
		            } else {
		                System.out.println("Failed to acquire lock!");
		            }
	    		}else{
	    			System.out.println("Seat Not Available..");
	    		}
			}
			//client.close();
        }catch(Exception e){
    		System.err.println("Exception at lockingProcess : "+e.getMessage());
    	}finally{
    		
    	}
        return lock_exits;
    }
    
}

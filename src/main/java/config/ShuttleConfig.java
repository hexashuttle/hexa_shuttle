package config;

public class ShuttleConfig {
	
	public static final String FAVOURITE="1";
	public static final String REMOVED_FROM_FAVOURITE="2";
	public static final String BOOKING_INITIATED="100";
	public static final String BOOKING_CONFIRMED="200";
	public static final String PICKUP_CONFIRMED_FOR_BOOKING="300";
	public static final String BOOKING_CANCEL="400";
	public static final String PAYMENT_INITIATED="200";
	public static final String PAYMENT_COMPLETED = "250";
	
	//============================= Booking State=================
	public static int INITIATED =100;
	public static int PICKED_UP = 200;
	public static int DROP = 300;
	public static int CANCEL_BY_PASSENGER = 400;
	public static int CANCEL_BY_DRIVER = 500;
	public static int inc_count = 1;
	//=============================== Trip State===================
	public static int TRIP_INITIATED =100;
	public static int ON_TRIP = 200;
	public static int TRIP_COMPLETED = 300;
	public static int TRIP_CANCEL= 400;
	//============================= Dynamo Table===================
	public static final String TABLE_CONFIG = getEnv()+"_config";//"t_config";
	public static final String TABLE_NOTIFICATION_CONFIG = getEnv()+"_notification_config";//"t_notification_config";
	public static final String TABLE_LOCK = getEnv()+"_lock";
	//============================= Api Success ===================
	public static String SREMARKS = "Successful";
	public static String FREMARKS = "Fail..";
	
	
	public static final String IS_SMS="is_sms";
	public static final String IS_PUSH="is_push";
	public static final String BOOKING_PUSH_MSG_TITLE="booking confirmed";
	public static final String CANCEL_BOOKING_PUSH_MSG_TITLE="cancel booking";
	public static final String BOOKING_ACTION="B";
	public static final String CANCEL_BOOKING_ACTION="CB";
	
	public static final String PASSENGER_PICK_PUSH_CODE="10";
	
	
	//==========================
	public static final String PASSENGER_TRIP_CREATED="100";
	public static final String PASSENGER_PICKED_UP="300";
	public static final String PASSENGER_TRIP_CANCELLED="400";
	public static final String PASSENGER_DROPPED="500";
	
	public static String getEnv(){
        String env = System.getenv("ENV");
        //System.out.println("ENV : "+env);
        if(env==null || env.equals("") || env.equals("null")){
        	env="t";
        }
        System.out.println("Environment running on : "+env);
        return env;

    }

}

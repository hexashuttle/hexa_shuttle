package config;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AppConfig {
	
	
	//private static final ALogger ilogger = Logger.of(AppConfig.class);
	//####### server url
	public static String HOST_PREFIX=getAppConfValue();
    //public static final String HOST_PREFIX="http://sso.hexarides.com";//production server
	//public static final String HOST_PREFIX="http://test.hexarides.com";//test server
    public static final String SIGN_UP_URL=HOST_PREFIX+"/api/sign_up/";
    public static final String LOGIN_URL=HOST_PREFIX+"/api/login/";
    public static final String PUSH_MSG_URL=HOST_PREFIX+"/api/push/";
    public static final String SMS_URL=HOST_PREFIX+"/api/push/";
    public static final String SMS_MSG_URL=HOST_PREFIX+"/api/get_otp/";
    public static final String VALIADTE_SMS_MSG_URL=HOST_PREFIX+"/api/validate_otp/";
    public static final String USER_TRACK_MY_CAR=HOST_PREFIX+"/api/user_track_my_car/";
    public static final String USER_STATE=HOST_PREFIX+"/api/user_state/";
    public static final String RECIEVE_RESET_OTP=HOST_PREFIX+"/api/rcv_rst_otp/";//api/rcv_rst_otp/?mobile=123456
    public static final String RESET_PASSWORD=HOST_PREFIX+"/api/reset/";
    
   
    public static final String TOKEN_URL=HOST_PREFIX+"/o/token/";
    public static final String REVOKE_TOKEN_URL=HOST_PREFIX+"/o/revoke_token/";
    public static final String EXPIRE_TOKEN_ACCESS=HOST_PREFIX+"/api/expire_token/acess";
    public static final String PASSWORD_CHANGE=HOST_PREFIX+"/api/passchange/";
    public static final String PASSWORD_CHANGE_SECONDARY=HOST_PREFIX+"/api/enckey/";
    public static final String NEARBY_URL=HOST_PREFIX+"/cabs/nearby";
    public static final String SHARED_URL=HOST_PREFIX+"/cabs/shared";
    public static final String BOOK_CAB_URL=HOST_PREFIX+"/cabs/vehicle_req_not_in_trip";
    public static final String UPDATE_CAB_LOC_URL=HOST_PREFIX+"/cabs/update_loc/9/";
    public static final String LINK_EXIT=HOST_PREFIX+"/p/link_exist/u";
    public static final String NEED_SOLUTION=HOST_PREFIX+"/p/need_sol/";
    public static final String UPGRADE_TO_HEXA=HOST_PREFIX+"/p/upgrade/";
    public static final String P_DASSHBOARD=HOST_PREFIX+"/p/dashboard2/";//s_id=60 it is changed to dashboard2
    
    public static final String DATA_COLLECT=HOST_PREFIX+"/p/datacollect/";
    public static final String UN_ACCESS=HOST_PREFIX+"/unAccess";
    public static final String STATUS_CODE=HOST_PREFIX+"/p/statusCode";
    public static final String DRIVER_STATE_FROM_PARENT_URL=HOST_PREFIX+"/p/mydriver/";
    
    //Image url in amazon s3
    public static final String DISPLAY_PICTURE="https://s3-ap-southeast-1.amazonaws.com/hexa-doc/dp/";
    public static final String STATIC_DP="https://s3-ap-southeast-1.amazonaws.com/hexa-doc/dp/default.jpg";
    
    //push message google url
    public static final String NEW_PUSH_MSG_URL="https://fcm.googleapis.com/fcm/send";
    
    //Pick and Drop operation flag
    public static final String READY_TO_PICKUP_FROM_HOME="0";
    public static final String ONRIDE_FROM_HOME="1";
    public static final String RIDE_COMPLETE_FROM_HOME="2"; //TODO implement later with 2
    public static final String READY_TO_PICKUP_FROM_SCHOOL="2";
    public static final String ONRIDE_FROM_SCHOOL="4";
    /**
     * Student dropped at home from school
     */
    public static final String STUDENT_DROPPED_AT_HOME="5";
    public static final String RIDE_COMPLETE_FROM_SCHOOL="0";
    public static final String RIDER_ABSENT="10";//need to delete
    public static final String ABSENT_BY_DRIVER="10";
    public static final String ABSENT_BY_PARENT="11";
    public static final String CANCEL_BY_DRIVER="11";//need to delete
    
    public static final String DRIVER_TRIP_END="99";
    /**
     * Driver has ended the trip
     */
    public static final String DRIVER_OFFLINE="99";
    /**
     * Shift is offline
     */
    public static final String SHIFT_OFFLINE="98";
    
    public static final String DRIVER_READY_TO_PICKUP_FROM_HOME="0";
    
    
    public static final String API_TRACK_ONRIDE="1";
    public static final String API_TRACK_RIDE_COMPLETED="2";
    public static final String BOTH_WAY="0";
    public static final String HOME_TO_SCHOOL="1";
    public static final String SCHOOL_TO_HOME="2";
    
    public static final String TRIP_CONTINUE="1";
    public static final String TRIP_COMPLETE="2";
    public static final String DROP_DEFAULT_VALUE="0101000020E610000000000000000022C000000000000022C0";//latitide=-9,longitude=-9
    public static final String STATIC_DROP_POINT="-9.0";
    
    //#### for sms service
    public static final String PICK="P";
    public static final String DROP="D";
    public static final String HOME="home";
    public static final String SCHOOL="school";
    public static final String PICK_UP_STARTED_FROM_HOME="1";
    public static final String DROPPED_ALL_AT_SCHOOL="2";
    public static final String PICK_UP_STARTED_FROM_SCHOOL="3";
    public static final String DROPPED_ALL_AT_HOME="4";
    public static final String DROPPED__AT_SCHOOL="2";
    public static final String DROPPED__AT_HOME="0";
    public static final String DRIVER_DROPPED_ALL_AT_HOME="5";
    public static final String DRIVER_DROPPED_ALL_AT_SCHOOL="2";
   
    public static final String DRIVER_DROPPED_ALL_STUDENT_AT_HOME="5";
    
    public static final String PICKUP_PUSH_MSG="Rider has been picked up.";
    public static final String PICKUP_PUSH_MSG_TITLE="Pickup Message";
    public static final String PICKUP_SHORT_MESSAGE_SERVICE_TITLE="Pickup SMS";
    public static final String DROP_PUSH_MSG="Rider has been dropped.";
    public static final String DROP_PUSH_MSG_TITLE="Drop Message";
    public static final String START_TRIP_MSG="Trip Started";
    public static final boolean SMS=true;
    public static final boolean SMS_SERVICE_TO_PARENT=false;
    public static final boolean SMS_SERVICE_TO_OWNER=true;
    public static final String REPORT_DAY_INTERVAL="1";
    public static final boolean REPORT_CREATION=true;
    //App state code
    public static final Integer NONE_STATE=0;
    public static final Integer SIGNUP_COMPLETE_STATE=1;
    public static final Integer SMS_VALIDATION_COMPLETE_STATE=2;
    public static final Integer NEED_SOLUTION_STATE=3;
    public static final Integer UPGARDE_TO_HEXA=4;
    public static final Integer WAITING_FOR_APPROVAL=5;
    public static final Integer LEAD_CREATED=6;
    public static final Integer UPGRADE_TO_HEXA_COMPLETE=60;
    public static final Integer IN_DASH_BOARD=20;
    public static final Integer DASHBOARD_NOT_ALLOWED=59;
    public static final Integer NEED_PASSWORD_RESET=58;
    public static final Integer EMAIL_DOESNOT_EXIST=57;
    
    //Http Error Code
    public static final String DATA_NOT_FOUND="404";
    public static final String UNAUTHORIZE_ACCESS="401";
    public static final String BAD_REQUEST="400";
    public static final String OK="200";
    public static final String INTERNAL_SERVER_ERROR="500";
    //Http header msg
    public static final String BEARER="Bearer";
    public static final String BASIC="Basic";
    
    //role
    public static final String ROLE_ADMIN="Admin";
    public static final String ROLE_DRIVER="Driver";
    public static final String ROLE_USER="User";
    public static final String ROLE_PARENT="Parent";
    public static final String ROLE_STUDENT="Student";
    public static final String ROLE_Rider="Rider";
    public static final String ROLE_OWNER="Partner";
    public static final String SPACE_SEPARATOR_IN_SMS="%20";
    public static final String DRIVER_STATE_WHEN_NULL="-1";
    public static final String LOCAL="local";
    public static final String PRODUCTION="production";
    public static final String APP_URL="http://www.hexaride.com/track";
    public static final String ABSENT="A";
    public static final String CANCEL_ABSENT="CA";
    //public static final String ABSENT_BY_PARENT_P="A";
    //public static final String CANCEL_ABSENT_BY_PARENT="CA";
    
    public static final String SMS_ABSENT_TITLE="Absent Message";
    public static final String SMS_CANCEL_ABSENT_TITLE="Action Cancelled";
    public static final String PUSH_ABSENT_TITLE="Absent Message";
    public static final String PUSH_CANCEL_ABSENT_TITLE="Action Cancelled";
    public static final String PUSH_VIDEO_START="Video Start";
    public static final String PUSH_VIDEO_ACTION="VR_START";
    public static final String PICKUP="P";
    public static final String DROPPED="D";
    
    public static final String PARENT_TRIP_START_SMS="1";
    public static final String PARENT_TRIP_START_PUSH="1";
    
    public static final String PARENT_PICKUP_FROM_HOME_SMS="2";
    public static final String PARENT_PICKUP_FROM_HOME_PUSH="2";
    
    public static final String PARENT_DROPPED_AT_SCHOOL_SMS="3";
    public static final String PARENT_DROPPED_AT_SCHOOL_PUSH="3";
    
    public static final String PARENT_PICKUP_FROM_SCHOOL_SMS="4";
    public static final String PARENT_PICKUP_FROM_SCHOOL_PUSH="4";
    
    public static final String PARENT_DROPPED_AT_HOME_SMS="5";
    public static final String PARENT_DROPPED_AT_HOME_PUSH="5";
    
    public static final String DRIVER_ABSENT_SMS_TO_PARENT="6";
    public static final String DRIVER_ABSENT_PUSH_TO_PARENT="6";
    
    public static final String DRIVER_CANCEL_ABSENT_SMS_TO_PARENT="7";
    public static final String DRIVER_CANCEL_ABSENT_PUSH_TO_PARENT="7";
    
    public static final String PARENT_ABSENT_PUSH_TO_DRIVER="8";
    public static final String PARENT_CANCEL_ABSENT_PUSH_TO_DRIVER="9";
    
    public static final String OWNER_TRIP_START_SMS="10";
    public static final String OWNER_DRIVER_STARTED_PICKING_FROM_HOME="11";
    public static final String OWNER_DRIVER_DROPPED_ALL_AT_SCHOOL="12";
    public static final String OWNER_DRIVER_STARTED_PICKING_FROM_SCHOOL="13";
    public static final String OWNER_DRIVER_DROPPED_ALL_AT_HOME="14";
    public static final String OWNER_TRIP_END_SMS="15";
    public static final String PUSH_VIDEO_START_REQ="20";
    public static final String IS_SMS="sms";
    public static final String IS_PUSH="push";
    public static final String IS_EMAIL="email";
    
    public static final String SERVICE_DONE="1";
    public static final String TRIP_START_SERVICE="trip started";
    public static final String TRIP_END_SERVICE="trip ended";
    //public static final String TRIP_SERVICE_ENDED="1";
    //Driver Trip constant
    public static final Integer BEFORE_TRIP_START=1;
    public static final Integer TRIP_STARTED=2;
    public static final Integer DROP_ALL_AT_SCHOOL=3;
    public static final Integer PICKUP_STARTED_FROM_SCHOOL=4;
    public static final Integer DROP_ALL_AT_HOME=5;
    public static final Integer TRIP_ENDED=6;
    
    public static final String IMAGE_TYPE_FULL="full";
    public static final String IMAGE_TYPE_CROP="crop";
    
    public static final String MONGO_DEV_USERNAME="mongodb";
    public static final String MONGO_DEV_PASSWORD="";
    public static final String MONGO_DEV_HOST="139.59.23.151";
    public static final String MONGO_DEV_PORT="27017";
    
    public static final String MONGO_PROD_USERNAME="mongodb";
    public static final String MONGO_PROD_PASSWORD="";
    public static final String MONGO_PROD_HOST="139.59.23.151";
    public static final String MONGO_PROD_PORT="27017";
    
    
    public static final String MONGO_CLIENT_URL=getMongoClientUrl();
    public static final String MONGO_DB=getMongoDB();
    //public static final String MONGO_COLLECTION=getMongoCollection();
    public static final int UPDATE_PRIORITY=5;
    public static final int UPDATE_NOT_CRITICAL=0;
    public static final String CURRENT="status";
    public static final String NOT_IN_USE="not in use";
    public static boolean PROVIDE_PRIMARY_KEY=false;
    public static final Integer USER_APPROVED=301;
    public static final String USER_APPROVED_EMAIL="user approved.";
    public static final Integer BULK_EMAIL=302;
    public static final String NOT_APPLICABLE="N/A";
    public static final String RIDERS_COLLECTION_NAME="riders";
    public static final String DRIVER_COLLECTION_NAME="driver";
    
    public static final String SECONDARY_CREATED_SMS = "10";
    public static final String SECONDARY_NOT_CREATED_SMS = "11";
    public static final String SECONDARY_PARENT_CREATED_PUSHMSG = "Dual Parent Created";
    public static final String SECONDARY_CREATED_PUSH = "12";
    public static final String SECONDARY_PARENT_NOT_CREATED_PUSHMSG = "Dual Parent Creation Failed";
    public static final String SECONDARY_NOT_CREATED_PUSH = "13";
    public static final String SECONDARY_UPDATED_SMS = "15";
    
    @SuppressWarnings("deprecation")
    public static String getMongoDB() {
    	try {
    		String MONGO_DB="api";//System.getenv("MONGO_DB");
    		return MONGO_DB;
    	} catch(Exception e) {
    		System.err.println("Exception at getMongoDB="+e.getMessage());
    		return "";
    	}
    }
    
    @SuppressWarnings("deprecation")
    public static String getMongoClientUrl() {
    	String mongo_url=null;
    	try {
    		
    		String MONGO_HOST_PRIMARY="g91.hexarides.com";//System.getenv("MONGO_HOST_PRIMARY");
    		String MONGO_HOST_SECONDARY="g92.hexarides.com";//System.getenv("MONGO_HOST_SECONDARY");
    		String MONGO_PORT="27017";//System.getenv("MONGO_PORT");
    		String MONGO_USERNAME="rpr";//System.getenv("MONGO_USERNAME");
    		String MONGO_PASSWORD="yWhJ630nIHVDkhPpInYZS9AK9PBnxVT7";//System.getenv("MONGO_PASSWORD");
    		String NO_SQL="mongodb";//System.getenv("NO_SQL");
    		String AUTH_SOURCE="admin";//System.getenv("AUTH_SOURCE");
    		String REPLICA_SET="rp0";//System.getenv("REPLICA_SET");
    		mongo_url=NO_SQL+"://"+MONGO_USERNAME+":"+MONGO_PASSWORD+"@"+MONGO_HOST_PRIMARY+":"+MONGO_PORT+","+MONGO_HOST_SECONDARY+":"+MONGO_PORT+"/?authSource="+AUTH_SOURCE+"&replicaSet="+REPLICA_SET;
    		System.out.println("MongoDb connection url= "+mongo_url);
    		return mongo_url;
    	} catch(Exception e) {
    		System.err.println("Exception at getMongoClientUrl="+e.getMessage());
    		return "";
    	}
    }
    @SuppressWarnings("deprecation")
	public static String getAppConfValue()
    {
    	String url="";
    	try {
    		url="";//System.getenv("PYTHON_URL");
    		System.out.println("AppConfig Access Url="+url);
    	} catch(Exception e) {
    		System.err.println("Exception in Appconfig="+e.getMessage());
    	}
    	return url; 
    }
    
    public static String getServerDateTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); //2014-08-06 15:59:48
		return String.valueOf(dateFormat.format(date));
	}
    
    
}


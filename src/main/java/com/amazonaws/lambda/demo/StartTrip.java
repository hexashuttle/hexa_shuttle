package com.amazonaws.lambda.demo;

//import org.json.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import controller.Driver;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import mongo.DashBoard;
import request.RequestDetails;

public class StartTrip implements RequestHandler<RequestDetails, Object> {

	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject handleRequest(RequestDetails reqObj, Context context) {
		context.getLogger().log("Input: " + reqObj.getEmp_id());
	    // TODO: implement your handler
	    JSONObject json = new JSONObject();
	    json.put("emp_id", reqObj.getEmp_id());
	    json.put("emp_name", reqObj.getEmp_name());
	    System.out.println("Obj  size : "+json.size());
		System.out.println("AWS_LAMBDA_FUNCTION_NAME : " + System.getenv("AWS_LAMBDA_FUNCTION_NAME"));
		System.out.println("HEXA_DB : " + System.getenv("HEXA_DB"));
		Driver o=new Driver();
		//o.getMobileNumber("401");
		RestApi oo=new RestApi();
		oo.testRestApi();
		
		return json;
	 }
}

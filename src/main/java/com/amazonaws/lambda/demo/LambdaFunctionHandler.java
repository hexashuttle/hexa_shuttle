package com.amazonaws.lambda.demo;

//import org.json.JSONObject;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.io.IOException;
import java.io.StringWriter;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import mongo.DashBoard;

public class LambdaFunctionHandler implements RequestHandler<Object, Object> {

	
	@Override
	public JSONObject handleRequest(Object input, Context context) {
		context.getLogger().log("Input: " + input);

	    // TODO: implement your handler
	    //RestApi obj=new RestApi();
	    //obj.testRestApi();
	    String email="satyadriver@hexa.com";
	    String student_id="1";
	        
	    DashBoard dBoardObj= new DashBoard();
	    String dash_response=dBoardObj.getViaPoint(email, student_id);
	    JSONParser parser = new JSONParser();
	    JSONObject json = null;
		try {
			json = (JSONObject) parser.parse(dash_response);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("AWS_LAMBDA_FUNCTION_NAME : " + System.getenv("AWS_LAMBDA_FUNCTION_NAME"));
		System.out.println("HEXA_DB : " + System.getenv("HEXA_DB"));
	    System.out.println("Obj  size : "+json.size());
	    return json;
	 }
	

}

package com.amazonaws.lambda.demo;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class RestApi {
	
	public void testRestApi(){
        System.out.println("Test rest api");
        try{
            HttpResponse<String> response = Unirest.get("https://nxt-prod.hexaride.com/count")
            		  .asString();
            System.out.println("Status: "+response.getStatus()+" Body : "+response.getBody());
            
        }catch(UnirestException e){
            System.err.println("===="+e.getMessage());
        }
	}
}
